################################################################################
# guard to make sure that OpenCL is selected as API
################################################################################

CORRECTAPI	:= 0
ifeq ($(API), OPENCL)
CORRECTAPI	:= 1
endif

################################################################################
# paths, directories and folders
################################################################################

GPUINSTALLPATH		:=	/opt/AMDAPPSDK-3.0
CLBLASINSTALLPATH	:=	external/atsccs69/clblas

CXXINCLUDES			:=	
CXXLIBDIR			:=	-L$(GPUINSTALLPATH)/lib/x86_64 \
						-Wl,-rpath=$(GPUINSTALLPATH)/lib/x86_64
CXXLIB				:=	

################################################################################
# compilers and linkers
################################################################################

CXX					:=	g++
LINKER				:=	g++

################################################################################
# compiler arguments and flags
################################################################################

CXXFLAGS			:=	

################################################################################
# linker arguments and flags
################################################################################

LINKERFLAGS			:=	

include common.make

