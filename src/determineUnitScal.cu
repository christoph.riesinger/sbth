/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "determineUnitScal.cuh"

template<class T>
__global__ void determineUnitScalKernel(T* norm, T* scal)
{
	T s;

	// scal = sqrt(2) / norm
	s = M_SQRT2 / norm[0];

	scal[0] = s;
}

template __global__ void determineUnitScalKernel<float>(float* norm, float* scal);
template __global__ void determineUnitScalKernel<double>(double* norm, double* scal);
