#ifndef CL_VERSION_1_2
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

kernel void computeDummySKernel(
		global float* data,
		const int offData,
		const int numOfIterations)
{
	size_t idx;
	float x;

	idx = get_local_id(1) * get_local_size(0) + get_local_id(0);
	x = data[offData + (idx % get_local_size(0))];

	for (int i = 0; i < numOfIterations; i++)
		x += -1.0f + (i & 1) * 2.0f;

	if (get_local_id(1) == 0)
		data[offData + idx] = x;
}

kernel void computeDummyDKernel(
		global double* data,
		const int offData,
		const int numOfIterations)
{
	size_t idx;
	double x;

	idx = get_local_id(1) * get_local_size(0) + get_local_id(0);
	x = data[offData + (idx % get_local_size(0))];

	for (int i = 0; i < numOfIterations; i++)
		x += -1.0 + (i & 1) * 2.0;

	if (get_local_id(1) == 0)
		data[offData + idx] = x;
}
