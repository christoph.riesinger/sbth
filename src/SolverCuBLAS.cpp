/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "SolverCuBLAS.hpp"

#include <cassert>

#include "common.h"
#include "Matrix.hpp"
#include "CudaInterface.cuh"

template <class T>
SolverCuBLAS<T>::SolverCuBLAS(const int numOfStreams) :
		numOfStreams(numOfStreams)
{
	CUBLAS_ERROR_CHECK(cublasCreate(&handle));
	CUBLAS_ERROR_CHECK(cublasSetPointerMode(handle, CUBLAS_POINTER_MODE_DEVICE));
	CUBLAS_ERROR_CHECK(cublasSetAtomicsMode(handle, CUBLAS_ATOMICS_NOT_ALLOWED));

	streams = new std::vector<cudaStream_t>(this->numOfStreams);

	for (int i = 0; i < this->numOfStreams; i++)
		CUDA_ERROR_CHECK(cudaStreamCreateWithFlags(&streams->at(i), cudaStreamNonBlocking));
}

template <class T>
SolverCuBLAS<T>::~SolverCuBLAS()
{
	/*
	for (int i = numOfStreams - 1; i >= 0; i--)
		CUDA_ERROR_CHECK(cudaStreamDestroy(streams->at(i)))
	*/

	delete streams;
	CUBLAS_ERROR_CHECK(cublasDestroy(handle));
}

template <class T>
void SolverCuBLAS<T>::getVector(int n, T* device, size_t offDevice, int incDevice, T* host, int incHost, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasGetVector(
			n,
			sizeof(T),
			&device[offDevice],
			incDevice,
			host,
			incHost));
}

template <class T>
void SolverCuBLAS<T>::setVector(int n, T* host, int incHost, T* device, size_t offDevice, int incDevice, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSetVector(
			n,
			sizeof(T),
			host,
			incHost,
			&device[offDevice],
			incDevice));
}

template <>
void SolverCuBLAS<float>::axpy(int n, float* alpha, size_t offAlpha, float* x, size_t offX, int incX, float* y, size_t offY, int incY, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSaxpy(
			handle,
			n,
			&alpha[offAlpha],
			&x[offX],
			incX,
			&y[offY],
			incY));
}

template <>
void SolverCuBLAS<double>::axpy(int n, double* alpha, size_t offAlpha, double* x, size_t offX, int incX, double* y, size_t offY, int incY, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDaxpy(
			handle,
			n,
			&alpha[offAlpha],
			&x[offX],
			incX,
			&y[offY],
			incY));
}

template <>
void SolverCuBLAS<float>::copy(int n, float* from, size_t offFrom, int incFrom, float* to, size_t offTo, int incTo, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasScopy(
			handle,
			n,
			&from[offFrom],
			incFrom,
			&to[offTo],
			incTo));
}

template <>
void SolverCuBLAS<double>::copy(int n, double* from, size_t offFrom, int incFrom, double* to, size_t offTo, int incTo, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDcopy(
			handle,
			n,
			&from[offFrom],
			incFrom,
			&to[offTo],
			incTo));
}

template <>
void SolverCuBLAS<float>::dot(int n, float* x, size_t offX, int incX, float* y, size_t offY, int incY, float* result, size_t offResult, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSdot(
			handle,
			n,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&result[offResult]));
}

template <>
void SolverCuBLAS<double>::dot(int n, double* x, size_t offX, int incX, double* y, size_t offY, int incY, double* result, size_t offResult, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDdot(
			handle,
			n,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&result[offResult]));
}

template <>
void SolverCuBLAS<float>::nrm2(int n, float* x, size_t offX, int incX, float* result, size_t offResult, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSnrm2(
			handle,
			n,
			&x[offX],
			incX,
			&result[offResult]));
}

template <>
void SolverCuBLAS<double>::nrm2(int n, double* x, size_t offX, int incX, double* result, size_t offResult, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDnrm2(
			handle,
			n,
			&x[offX],
			incX,
			&result[offResult]));
}

template <>
void SolverCuBLAS<float>::scal(int n, float* alpha, size_t offAlpha, float* x, size_t offX, int incX, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSscal(
			handle,
			n,
			&alpha[offAlpha],
			&x[offX],
			incX));
}

template <>
void SolverCuBLAS<double>::scal(int n, double* alpha, size_t offAlpha, double* x, size_t offX, int incX, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDscal(
			handle,
			n,
			&alpha[offAlpha],
			&x[offX],
			incX));
}

template <>
void SolverCuBLAS<float>::gemv(int m, int n, float* alpha, float* A, size_t offA, int lda, float* x, size_t offX, int incX, float* beta, float* y, size_t offY, int incY, MatrixOperation matrixOperation, int streamID)
{
	assert(streamID < numOfStreams);

	cublasOperation_t cublasOperation;

	switch (matrixOperation) {
		case NORMAL:
			cublasOperation = CUBLAS_OP_N;
			break;
		case TRANSPOSE:
			cublasOperation = CUBLAS_OP_T;
			break;
		default:
			cublasOperation = CUBLAS_OP_N;
			break;
	}

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSgemv(
			handle,
			cublasOperation,
			m,
			n,
			alpha,
			&A[offA],
			lda,
			&x[offX],
			incX,
			beta,
			&y[offY],
			incY));

	if (streamID >= 0)
		CudaInterface<float>::computeDummy(n, &x[offX], &streams->at(streamID));
	else
		CudaInterface<float>::computeDummy(n, &x[offX]);
}

template <>
void SolverCuBLAS<double>::gemv(int m, int n, double* alpha, double* A, size_t offA, int lda, double* x, size_t offX, int incX, double* beta, double* y, size_t offY, int incY, MatrixOperation matrixOperation, int streamID)
{
	assert(streamID < numOfStreams);

	cublasOperation_t cublasOperation;

	switch (matrixOperation) {
		case NORMAL:
			cublasOperation = CUBLAS_OP_N;
			break;
		case TRANSPOSE:
			cublasOperation = CUBLAS_OP_T;
			break;
		default:
			cublasOperation = CUBLAS_OP_N;
			break;
	}

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDgemv(
			handle,
			cublasOperation,
			m,
			n,
			alpha,
			&A[offA],
			lda,
			&x[offX],
			incX,
			beta,
			&y[offY],
			incY));

	if (streamID >= 0)
		CudaInterface<double>::computeDummy(n, &x[offX], &streams->at(streamID));
	else
		CudaInterface<double>::computeDummy(n, &x[offX]);
}

template <>
void SolverCuBLAS<float>::ger(int m, int n, float* alpha, float* x, size_t offX, int incX, float* y, size_t offY, int incY, float* A, size_t offA, int lda, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSger(
			handle,
			m,
			n,
			alpha,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda));

	if (streamID >= 0)
		CudaInterface<float>::computeDummy(n, &x[offX], &streams->at(streamID));
	else
		CudaInterface<float>::computeDummy(n, &x[offX]);
}

template <>
void SolverCuBLAS<double>::ger(int m, int n, double* alpha, double* x, size_t offX, int incX, double* y, size_t offY, int incY, double* A, size_t offA, int lda, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDger(
			handle,
			m,
			n,
			alpha,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda));

	if (streamID >= 0)
		CudaInterface<double>::computeDummy(n, &x[offX], &streams->at(streamID));
	else
		CudaInterface<double>::computeDummy(n, &x[offX]);
}

template <>
void SolverCuBLAS<float>::symv(int n, float* alpha, float* A, size_t offA, int lda, float* x, size_t offX, int incX, float* beta, float* y, size_t offY, int incY, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSsymv(
			handle,
			CUBLAS_FILL_MODE_LOWER,
			n,
			alpha,
			&A[offA],
			lda,
			&x[offX],
			incX,
			beta,
			&y[offY],
			incY));

	if (streamID >= 0)
		CudaInterface<float>::computeDummy(n, &x[offX], &streams->at(streamID));
	else
		CudaInterface<float>::computeDummy(n, &x[offX]);
}

template <>
void SolverCuBLAS<double>::symv(int n, double* alpha, double* A, size_t offA, int lda, double* x, size_t offX, int incX, double* beta, double* y, size_t offY, int incY, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDsymv(
			handle,
			CUBLAS_FILL_MODE_LOWER,
			n,
			alpha,
			&A[offA],
			lda,
			&x[offX],
			incX,
			beta,
			&y[offY],
			incY));

	if (streamID >= 0)
		CudaInterface<double>::computeDummy(n, &x[offX], &streams->at(streamID));
	else
		CudaInterface<double>::computeDummy(n, &x[offX]);
}

template <>
void SolverCuBLAS<float>::syr2(int n, float* alpha, float* x, size_t offX, int incX, float* y, size_t offY, int incY, float* A, size_t offA, int lda, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasSsyr2(
			handle,
			CUBLAS_FILL_MODE_LOWER,
			n,
			alpha,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda));

	if (streamID >= 0)
		CudaInterface<float>::computeDummy(n, &y[offY], &streams->at(streamID));
	else
		CudaInterface<float>::computeDummy(n, &y[offY]);
}

template <>
void SolverCuBLAS<double>::syr2(int n, double* alpha, double* x, size_t offX, int incX, double* y, size_t offY, int incY, double* A, size_t offA, int lda, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CUBLAS_ERROR_CHECK(cublasSetStream(handle, streams->at(streamID)));

	CUBLAS_ERROR_CHECK(cublasDsyr2(
			handle,
			CUBLAS_FILL_MODE_LOWER,
			n,
			alpha,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda));

	if (streamID >= 0)
		CudaInterface<double>::computeDummy(n, &y[offY], &streams->at(streamID));
	else
		CudaInterface<double>::computeDummy(n, &y[offY]);
}

template <class T>
void SolverCuBLAS<T>::determineHouseholderScal(T* x0, size_t offX0, T* norm, size_t offNorm, T* betaMinus, size_t offBetaMinus, T* scal, size_t offScal, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CudaInterface<T>::determineHouseholderScal(&x0[offX0], &norm[offNorm], &betaMinus[offBetaMinus], &scal[offScal], &streams->at(streamID));
	else
		CudaInterface<T>::determineHouseholderScal(&x0[offX0], &norm[offNorm], &betaMinus[offBetaMinus], &scal[offScal]);
}

template <class T>
void SolverCuBLAS<T>::determineUnitScal(T* norm, size_t offNorm, T* scal, size_t offScal, int streamID)
{
	assert(streamID < numOfStreams);

	if (streamID >= 0)
		CudaInterface<T>::determineUnitScal(&norm[offNorm], &scal[offScal], &streams->at(streamID));
	else
		CudaInterface<T>::determineUnitScal(&norm[offNorm], &scal[offScal]);
}

template <class T>
void SolverCuBLAS<T>::synchronizeDevice()
{
	CUDA_ERROR_CHECK(cudaDeviceSynchronize());
}

template <class T>
T SolverCuBLAS<T>::getSize()
{
	T size;

	size = (T)0 * (T)sizeof(T);

	return size;
}

template class SolverCuBLAS<float>;
template class SolverCuBLAS<double>;
