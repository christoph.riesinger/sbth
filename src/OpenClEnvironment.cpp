/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "OpenClEnvironment.hpp"

#include <iostream>

#include "common.h"

OpenClEnvironment::OpenClEnvironment(cl_device_type deviceType)
{
	cl_int error;
	cl_platform_id* platforms;
	cl_uint platformCount;
	cl_device_id* devices;
	cl_uint deviceCount;
	
	cl_context_properties properties[3] = {CL_CONTEXT_PLATFORM, 0, 0};
	
	OPENCL_ERROR_CHECK(clGetPlatformIDs(0, NULL, &platformCount))
	platforms = new cl_platform_id[platformCount];
	OPENCL_ERROR_CHECK(clGetPlatformIDs(platformCount, platforms, NULL))
 
	for (cl_uint i = 0; i < platformCount; i++)
	{
		OPENCL_ERROR_CHECK(clGetDeviceIDs(platforms[i], deviceType, 0, NULL, &deviceCount))
		
		/*
		 * Always, the first suitable device (deviceType) of the platform is
		 * used. A potentially existing second device is not considered.
		 * Only the first platform with a suitable device (deviceType) is used.
		 */
		if (deviceCount > 0)
		{
			devices = new cl_device_id[deviceCount];
			OPENCL_ERROR_CHECK(clGetDeviceIDs(platforms[i], deviceType, deviceCount, devices, NULL))
			
			platform = platforms[i];
			device = devices[0];
			properties[1] = (cl_context_properties) platform;
			context = clCreateContext(properties, 1, &device, NULL, NULL, &error);
			OPENCL_ERROR_CHECK(error)
 
			delete devices;
			
			break;
		}

		if (i == platformCount - 1)
		{
			std::cerr << "----- OpenClEnvironment::OpenClEnvironment() -----" << std::endl;
			std::cerr << "No device of the requested device type could be found in the system." << std::endl;
			std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
			std::cerr << "--------------------------------------------------" << std::endl;
		}
	}
	
	delete[] platforms;
}

OpenClEnvironment::~OpenClEnvironment()
{
	OPENCL_ERROR_CHECK(clReleaseContext(context))
}

cl_device_id* OpenClEnvironment::getDevice()
{
	return &device;
}

cl_context* OpenClEnvironment::getContext()
{
	return &context;
}

