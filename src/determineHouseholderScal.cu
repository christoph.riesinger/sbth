/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "determineHouseholderScal.cuh"

template<class T>
__global__ void determineHouseholderScalKernel(T* x0, T* norm, T* betaMinus, T* scal)
{
	T s, t;

	// beta = sgn(x(0)) * norm
	s = ((T(0) < x0[0]) - (x0[0] < T(0))) * norm[0];
	// 1 / (x(0) + beta)
	t = (T)1 / (x0[0] + s);

	betaMinus[0] = -s;
	scal[0] = t;
}

template __global__ void determineHouseholderScalKernel<float>(float* x1, float* norm, float* betaMinus, float* scal);
template __global__ void determineHouseholderScalKernel<double>(double* x1, double* norm, double* betaMinus, double* scal);
