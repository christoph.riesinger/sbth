/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef SOLVERMAGMA_HPP
#define SOLVERMAGMA_HPP

#include <vector>
#include <cuda_runtime.h>
#include <magma_v2.h>

#include "Solver.hpp"

template<typename T>
class SolverMAGMA : public Solver<T>
{
private:
	int numOfQueues;
	std::vector<magma_queue_t>* queues;
	T* norms;
	T* dots;
	T* scals;
	T* alphas;

public:
	SolverMAGMA(const int numOfQueues);
	virtual ~SolverMAGMA();

	/*
	 * Helper functions for setting and getting data
	 */
	virtual void getVector(int n, T* device, size_t offDevice, int incDevice, T* host, int incHost, int queueID = -1);
	virtual void setVector(int n, T* host, int incHost, T* device, size_t offDevice, int incDevice, int queueID = -1);

	/*
	 * BLAS level 1 routines
	 */
	// vector addition
	virtual void axpy(int n, T* alpha, size_t offAlpha, T* x, size_t offX, int incX, T* y, size_t offY, int incY, int queueID = -1);
	// copy
	virtual void copy(int n, T* from, size_t offFrom, int incFrom, T* to, size_t offTo, int incTo, int queueID = -1);
	// dot product
	virtual void dot(int n, T* x, size_t offX, int incX, T* y, size_t offY, int incY, T* result, size_t offResult, int queueID = -1);
	// euclidean norm
	virtual void nrm2(int n, T* x, size_t offX, int incX, T* result, size_t offResult, int queueID = -1);
	// scalar-vector multiplication
	virtual void scal(int n, T* alpha, size_t offAlpha, T* x, size_t offX, int incX, int queueID = -1);

	/*
	 * BLAS level 2 routines
	 */
	// general matrix-vector product
	virtual void gemv(int m, int n, T* alpha, T* A, size_t offA, int lda, T* x, size_t offX, int incX, T* beta, T* y, size_t offY, int incY, MatrixOperation matrixOperation = NORMAL, int queueID = -1);
	// general rank-1 update
	virtual void ger(int m, int n, T* alpha, T* x, size_t offX, int incX, T* y, size_t offY, int incY, T* A, size_t offA, int lda, int queueID = -1);
	// symmetric matrix-vector product
	virtual void symv(int n, T* alpha, T* A, size_t offA, int lda, T* x, size_t offX, int incX, T* beta, T* y, size_t offY, int incY, int queueID = -1);
	// symmetric rank-2 update
	virtual void syr2(int n, T* alpha, T* x, size_t offX, int incX, T* y, size_t offY, int incY, T* A, size_t offA, int lda, int queueID = -1);

	/*
	 * Miscellaneous functions which are required for the SBTH algorithm but
	 * which can't be expressed by BLAS routines.
	 */
	virtual void determineHouseholderScal(T* x0, size_t offX0, T* norm, size_t offNorm, T* betaMinus, size_t offBetaMinus, T* scal, size_t offScal, int queueID = -1);
	virtual void determineUnitScal(T* norm, size_t offNorm, T* scal, size_t offScal, int queueID = -1);

	/*
	 *
	 */
	virtual void synchronizeDevice();

	/*
	 *
	 */
	virtual T getSize();
};

#endif
