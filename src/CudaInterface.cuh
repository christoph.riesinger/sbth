/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef CUDAINTERFACE_CUH
#define CUDAINTERFACE_CUH

#include <cuda_runtime.h>

template<typename T>
class CudaInterface
{
public:
	static void determineHouseholderScal(T* x0, T* norm, T* betaMinus, T* scal, cudaStream_t* stream = NULL);
	static void determineUnitScal(T* norm, T* scal, cudaStream_t* stream = NULL);
	static void computeDummy(const int n, T* data, cudaStream_t* stream = NULL);
};

#endif
