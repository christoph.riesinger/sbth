/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "CudaInterface.cuh"

#include "computeDummy.cuh"
#include "determineHouseholderScal.cuh"
#include "determineUnitScal.cuh"

#include "common.h"

template <class T>
void CudaInterface<T>::determineHouseholderScal(T* x0, T* norm, T* betaMinus, T* scal, cudaStream_t* stream)
{
	determineHouseholderScalKernel<T><<<1, 1, 0, ((stream == NULL) ? 0 : *stream)>>>(x0, norm, betaMinus, scal);
	CUDA_ERROR_CHECK(cudaPeekAtLastError());
}

template <class T>
void CudaInterface<T>::determineUnitScal(T* norm, T* scal, cudaStream_t* stream)
{
	determineUnitScalKernel<T><<<1, 1, 0, ((stream == NULL) ? 0 : *stream)>>>(norm, scal);
	CUDA_ERROR_CHECK(cudaPeekAtLastError());
}

template <class T>
void CudaInterface<T>::computeDummy(const int n, T* data, cudaStream_t* stream)
{
	/*
	const int numOfIterations = (1<<10);
	const dim3 blockConfiguration(n, 256 / n, 1);

	computeDummyKernel<T><<<1, blockConfiguration, 0, ((stream == NULL) ? 0 : *stream)>>>(data, numOfIterations);
	CUDA_ERROR_CHECK(cudaPeekAtLastError());
	*/
}

template class CudaInterface<float>;
template class CudaInterface<double>;
