/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "SolverMAGMA.hpp"

#include <cassert>
#include <cuda_runtime.h>

#include "common.h"
#include "CudaInterface.cuh"
#include "Matrix.hpp"

template <class T>
SolverMAGMA<T>::SolverMAGMA(const int numOfQueues) :
		numOfQueues(numOfQueues)
{
	magma_init();

	queues = new std::vector<magma_queue_t>(this->numOfQueues);

	for (int i = 0; i < this->numOfQueues; i++)
		magma_queue_create(0, &queues->at(i));

	dots = new T[this->numOfQueues];
	norms = new T[this->numOfQueues];
	scals = new T[this->numOfQueues];
	alphas = new T[this->numOfQueues];
}

template <class T>
SolverMAGMA<T>::~SolverMAGMA()
{
	delete alphas;
	delete scals;
	delete norms;
	delete dots;

	for (int i = numOfQueues - 1; i >= 0; i--)
		magma_queue_destroy(queues->at(i));

	delete queues;

	magma_finalize();
}

template <>
void SolverMAGMA<float>::getVector(int n, float* device, size_t offDevice, int incDevice, float* host, int incHost, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_sgetvector_async(
			n,
			&device[offDevice],
			incDevice,
			host,
			incHost,
			queues->at(queueID));
}

template <>
void SolverMAGMA<double>::getVector(int n, double* device, size_t offDevice, int incDevice, double* host, int incHost, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_dgetvector_async(
			n,
			&device[offDevice],
			incDevice,
			host,
			incHost,
			queues->at(queueID));
}

template <>
void SolverMAGMA<float>::setVector(int n, float* host, int incHost, float* device, size_t offDevice, int incDevice, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_ssetvector_async(
			n,
			host,
			incHost,
			&device[offDevice],
			incDevice,
			queues->at(queueID));
}

template <>
void SolverMAGMA<double>::setVector(int n, double* host, int incHost, double* device, size_t offDevice, int incDevice, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_dsetvector_async(
			n,
			host,
			incHost,
			&device[offDevice],
			incDevice,
			queues->at(queueID));
}

template <>
void SolverMAGMA<float>::axpy(int n, float* alpha, size_t offAlpha, float* x, size_t offX, int incX, float* y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_sgetvector_async(
			1,
			&alpha[offAlpha],
			1,
			&alphas[queueID],
			1,
			queues->at(queueID));
	magma_saxpy(
			n,
			alphas[queueID],
			&x[offX],
			incX,
			&y[offY],
			incY,
			queues->at(queueID));
}

template <>
void SolverMAGMA<double>::axpy(int n, double* alpha, size_t offAlpha, double* x, size_t offX, int incX, double* y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_dgetvector_async(
			1,
			&alpha[offAlpha],
			1,
			&alphas[queueID],
			1,
			queues->at(queueID));
	magma_daxpy(
			n,
			alphas[queueID],
			&x[offX],
			incX,
			&y[offY],
			incY,
			queues->at(queueID));
}

template <>
void SolverMAGMA<float>::copy(int n, float* from, size_t offFrom, int incFrom, float* to, size_t offTo, int incTo, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	/*
	 * If incFrom != 0 (broadcasting a value), magma_scopyvector_async()
	 * eventually calls cudaMemcpy2DAsync() which only accepts values
	 * incFrom > 0. Thus, an efficient workaround has to be applied.
	 */
	if (incFrom)
		magma_scopyvector_async(
				n,
				&from[offFrom],
				incFrom,
				&to[offTo],
				incTo,
				queues->at(queueID));
	else
		magma_sscal(
				n,
				0,
				&to[offTo],
				incTo,
				queues->at(queueID));
}

template <>
void SolverMAGMA<double>::copy(int n, double* from, size_t offFrom, int incFrom, double* to, size_t offTo, int incTo, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	/*
	 * If incFrom != 0 (broadcasting a value), magma_dcopyvector_async()
	 * eventually calls cudaMemcpy2DAsync() which only accepts values
	 * incFrom > 0. Thus, an efficient workaround has to be applied.
	 */
	if (incFrom)
		magma_dcopyvector_async(
				n,
				&from[offFrom],
				incFrom,
				&to[offTo],
				incTo,
				queues->at(queueID));
	else
		magma_dscal(
				n,
				0,
				&to[offTo],
				incTo,
				queues->at(queueID));
}

template <>
void SolverMAGMA<float>::dot(int n, float* x, size_t offX, int incX, float* y, size_t offY, int incY, float* result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	dots[queueID] = magma_sdot(
			n,
			&x[offX],
			incX,
			&y[offY],
			incY,
			queues->at(queueID));
	magma_ssetvector_async(
			1,
			&dots[queueID],
			1,
			&result[offResult],
			1,
			queues->at(queueID));
}

template <>
void SolverMAGMA<double>::dot(int n, double* x, size_t offX, int incX, double* y, size_t offY, int incY, double* result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	dots[queueID] = magma_ddot(
			n,
			&x[offX],
			incX,
			&y[offY],
			incY,
			queues->at(queueID));
	magma_dsetvector_async(
			1,
			&dots[queueID],
			1,
			&result[offResult],
			1,
			queues->at(queueID));
}

template <>
void SolverMAGMA<float>::nrm2(int n, float* x, size_t offX, int incX, float* result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	norms[queueID] = magma_snrm2(
			n,
			&x[offX],
			incX,
			queues->at(queueID));
	magma_ssetvector_async(
			1,
			&norms[queueID],
			1,
			&result[offResult],
			1,
			queues->at(queueID));
}

template <>
void SolverMAGMA<double>::nrm2(int n, double* x, size_t offX, int incX, double* result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	norms[queueID] = magma_dnrm2(
			n,
			&x[offX],
			incX,
			queues->at(queueID));
	magma_dsetvector_async(
			1,
			&norms[queueID],
			1,
			&result[offResult],
			1,
			queues->at(queueID));
}

template <>
void SolverMAGMA<float>::scal(int n, float* alpha, size_t offAlpha, float* x, size_t offX, int incX, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_sgetvector_async(
			1,
			&alpha[offAlpha],
			1,
			&scals[queueID],
			1,
			queues->at(queueID));
	magma_sscal(
			n,
			scals[queueID],
			&x[offX],
			incX,
			queues->at(queueID));
}

template <>
void SolverMAGMA<double>::scal(int n, double* alpha, size_t offAlpha, double* x, size_t offX, int incX, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	magma_dgetvector_async(
			1,
			&alpha[offAlpha],
			1,
			&scals[queueID],
			1,
			queues->at(queueID));
	magma_dscal(
			n,
			scals[queueID],
			&x[offX],
			incX,
			queues->at(queueID));
}

template <>
void SolverMAGMA<float>::gemv(int m, int n, float* alpha, float* A, size_t offA, int lda, float* x, size_t offX, int incX, float* beta, float* y, size_t offY, int incY, MatrixOperation matrixOperation, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_trans_t magmaTrans;

	switch (matrixOperation) {
		case NORMAL:
			magmaTrans = MagmaNoTrans;
			break;
		case TRANSPOSE:
			magmaTrans = MagmaTrans;
			break;
		default:
			magmaTrans = MagmaNoTrans;
			break;
	}

	magma_sgemv(
			magmaTrans,
			m,
			n,
			1,
			&A[offA],
			lda,
			&x[offX],
			incX,
			0,
			&y[offY],
			incY,
			queues->at(queueID));

	CudaInterface<float>::computeDummy(n, &x[offX], &stream);
}

template <>
void SolverMAGMA<double>::gemv(int m, int n, double* alpha, double* A, size_t offA, int lda, double* x, size_t offX, int incX, double* beta, double* y, size_t offY, int incY, MatrixOperation matrixOperation, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_trans_t magmaTrans;

	switch (matrixOperation) {
		case NORMAL:
			magmaTrans = MagmaNoTrans;
			break;
		case TRANSPOSE:
			magmaTrans = MagmaTrans;
			break;
		default:
			magmaTrans = MagmaNoTrans;
			break;
	}

	magma_dgemv(
			magmaTrans,
			m,
			n,
			1,
			&A[offA],
			lda,
			&x[offX],
			incX,
			0,
			&y[offY],
			incY,
			queues->at(queueID));

	CudaInterface<double>::computeDummy(n, &x[offX], &stream);
}

template <>
void SolverMAGMA<float>::ger(int m, int n, float* alpha, float* x, size_t offX, int incX, float* y, size_t offY, int incY, float* A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_sger(
			m,
			n,
			-1,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda,
			queues->at(queueID));

	CudaInterface<float>::computeDummy(n, &x[offX], &stream);
}

template <>
void SolverMAGMA<double>::ger(int m, int n, double* alpha, double* x, size_t offX, int incX, double* y, size_t offY, int incY, double* A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_dger(
			m,
			n,
			-1,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda,
			queues->at(queueID));

	CudaInterface<double>::computeDummy(n, &x[offX], &stream);
}

template <>
void SolverMAGMA<float>::symv(int n, float* alpha, float* A, size_t offA, int lda, float* x, size_t offX, int incX, float* beta, float* y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_ssymv(
			MagmaLower,
			n,
			1,
			&A[offA],
			lda,
			&x[offX],
			incX,
			0,
			&y[offY],
			incY,
			queues->at(queueID));

	CudaInterface<float>::computeDummy(n, &x[offX], &stream);
}

template <>
void SolverMAGMA<double>::symv(int n, double* alpha, double* A, size_t offA, int lda, double* x, size_t offX, int incX, double* beta, double* y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_dsymv(
			MagmaLower,
			n,
			1,
			&A[offA],
			lda,
			&x[offX],
			incX,
			0,
			&y[offY],
			incY,
			queues->at(queueID));

	CudaInterface<double>::computeDummy(n, &x[offX], &stream);
}

template <>
void SolverMAGMA<float>::syr2(int n, float* alpha, float* x, size_t offX, int incX, float* y, size_t offY, int incY, float* A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_ssyr2(
			MagmaLower,
			n,
			-1,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda,
			queues->at(queueID));

	CudaInterface<float>::computeDummy(n, &y[offY], &stream);
}

template <>
void SolverMAGMA<double>::syr2(int n, double* alpha, double* x, size_t offX, int incX, double* y, size_t offY, int incY, double* A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

	magma_dsyr2(
			MagmaLower,
			n,
			-1,
			&x[offX],
			incX,
			&y[offY],
			incY,
			&A[offA],
			lda,
			queues->at(queueID));

	CudaInterface<double>::computeDummy(n, &y[offY], &stream);
}

template <class T>
void SolverMAGMA<T>::determineHouseholderScal(T* x0, size_t offX0, T* norm, size_t offNorm, T* betaMinus, size_t offBetaMinus, T* scal, size_t offScal, int queueID)
{
	assert(queueID < numOfQueues);

	if (queueID >= 0)
	{
		cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

		CudaInterface<T>::determineHouseholderScal(&x0[offX0], &norm[offNorm], &betaMinus[offBetaMinus], &scal[offScal], &stream);
	}
	else
	{
		CudaInterface<T>::determineHouseholderScal(&x0[offX0], &norm[offNorm], &betaMinus[offBetaMinus], &scal[offScal]);
	}
}

template <class T>
void SolverMAGMA<T>::determineUnitScal(T* norm, size_t offNorm, T* scal, size_t offScal, int queueID)
{
	assert(queueID < numOfQueues);

	if (queueID >= 0)
	{
		cudaStream_t stream = magma_queue_get_cuda_stream(queues->at(queueID));

		CudaInterface<T>::determineUnitScal(&norm[offNorm], &scal[offScal], &stream);
	} else {
		CudaInterface<T>::determineUnitScal(&norm[offNorm], &scal[offScal]);
	}
}

template <class T>
void SolverMAGMA<T>::synchronizeDevice()
{
	CUDA_ERROR_CHECK(cudaDeviceSynchronize());
}

template <class T>
T SolverMAGMA<T>::getSize()
{
	T size;

	size = (T)0 * (T)sizeof(T);

	return size;
}

template class SolverMAGMA<float>;
template class SolverMAGMA<double>;
