#ifndef CL_VERSION_1_2
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

kernel void determineUnitScalSKernel(
		global float* norm,
		const int offNorm,
		global float* scal,
		const int offScal)
{
	float s;

	// scal = sqrt(2) / norm
	s = M_SQRT2 / norm[offNorm];

	scal[offScal] = s;
}

kernel void determineUnitScalDKernel(
		global double* norm,
		const int offNorm,
		global double* scal,
		const int offScal)
{
	double s;

	// scal = sqrt(2) / norm
	s = M_SQRT2 / norm[offNorm];

	scal[offScal] = s;
}

