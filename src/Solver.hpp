/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef SOLVER_HPP
#define SOLVER_HPP

#include "common.h"
#include "Matrix.hpp"

#include <cstddef>
#ifdef OPENCL
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <CL/cl.h>

#include "OpenClEnvironment.hpp"
#endif

#ifdef OPENCL
#define MEMORY_TYPE cl_mem
#else
#define MEMORY_TYPE	T*
#endif

template<typename T>
class Solver
{
public:
	virtual ~Solver() {}

	/*
	 * Helper functions for setting and getting data
	 */
	virtual void getVector(int n, MEMORY_TYPE device, size_t offDevice, int incDevice, T* host, int incHost, int queueID = -1) = 0;
	virtual void setVector(int n, T* host, int incHost, MEMORY_TYPE device, size_t offDevice, int incDevice, int queueID = -1) = 0;

	/*
	 * BLAS level 1 routines
	 */
	// vector addition
	virtual void axpy(int n, MEMORY_TYPE alpha, size_t offAlpha, MEMORY_TYPE x, size_t offX, int incX, MEMORY_TYPE y, size_t offY, int incY, int queueID = -1) = 0;
	// copy
	virtual void copy(int n, MEMORY_TYPE from, size_t offFrom, int incFrom, MEMORY_TYPE to, size_t offTo, int incTo, int queueID = -1) = 0;
	// dot product
	virtual void dot(int n, MEMORY_TYPE x, size_t offX, int incX, MEMORY_TYPE y, size_t offY, int incY, MEMORY_TYPE result, size_t offResult, int queueID = -1) = 0;
	// euclidean norm
	virtual void nrm2(int n, MEMORY_TYPE x, size_t offX, int incX, MEMORY_TYPE result, size_t offResult, int queueID = -1) = 0;
	// scalar-vector multiplication
	virtual void scal(int n, MEMORY_TYPE alpha, size_t offAlpha, MEMORY_TYPE x, size_t offX, int incX, int queueID = -1) = 0;

	/*
	 * BLAS level 2 routines
	 */
	// general matrix-vector product
	virtual void gemv(int m, int n, MEMORY_TYPE alpha, MEMORY_TYPE A, size_t offA, int lda, MEMORY_TYPE x, size_t offX, int incX, MEMORY_TYPE beta, MEMORY_TYPE y, size_t offY, int incY, MatrixOperation matrixOperation = NORMAL, int queueID = -1) = 0;
	// general rank-1 update
	virtual void ger(int m, int n, MEMORY_TYPE alpha, MEMORY_TYPE x, size_t offX, int incX, MEMORY_TYPE y, size_t offY, int incY, MEMORY_TYPE A, size_t offA, int lda, int queueID = -1) = 0;
	// symmetric matrix-vector product
	virtual void symv(int n, MEMORY_TYPE alpha, MEMORY_TYPE A, size_t offA, int lda, MEMORY_TYPE x, size_t offX, int incX, MEMORY_TYPE beta, MEMORY_TYPE y, size_t offY, int incY, int queueID = -1) = 0;
	// symmetric rank-2 update
	virtual void syr2(int n, MEMORY_TYPE alpha, MEMORY_TYPE x, size_t offX, int incX, MEMORY_TYPE y, size_t offY, int incY, MEMORY_TYPE A, size_t offA, int lda, int queueID = -1) = 0;

	/*
	 * Miscellaneous functions which are required for the SBTH algorithm but
	 * which can't be expressed by BLAS routines.
	 */
	virtual void determineHouseholderScal(MEMORY_TYPE x0, size_t offX0, MEMORY_TYPE norm, size_t offNorm, MEMORY_TYPE betaMinus, size_t offBetaMinus, MEMORY_TYPE scal, size_t offScal, int queueID = -1) = 0;
	virtual void determineUnitScal(MEMORY_TYPE norm, size_t offNorm, MEMORY_TYPE scal, size_t offScal, int queueID = -1) = 0;

	/*
	 * Helper functions being relevant in the context of GPUs since the solver
	 * runs on the device.
	 */
	virtual void synchronizeDevice() = 0;

	/*
	 *
	 */
	virtual T getSize() = 0;
};

#endif
