#ifndef CL_VERSION_1_2
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#endif

kernel void determineHouseholderScalSKernel(
		global float* x0,
		const int offX0,
		global float* norm,
		const int offNorm,
		global float* betaMinus,
		const int offBetaMinus,
		global float* scal,
		const int offScal)
{
	float s, t;

	// beta = sgn(x(0)) * norm
	s = ((0.0f < x0[offX0]) - (x0[offX0] < 0.0f)) * norm[offNorm];
	// 1 / (x(0) + beta)
	t = 1.0f / (x0[offX0] + s);

	betaMinus[offBetaMinus] = -s;
	scal[offScal] = t;
}

kernel void determineHouseholderScalDKernel(
		global double* x0,
		const int offX0,
		global double* norm,
		const int offNorm,
		global double* betaMinus,
		const int offBetaMinus,
		global double* scal,
		const int offScal)
{
	double s, t;

	// beta = sgn(x(0)) * norm
	s = ((0.0 < x0[offX0]) - (x0[offX0] < 0.0)) * norm[offNorm];
	// 1 / (x(0) + beta)
	t = 1.0 / (x0[offX0] + s);

	betaMinus[offBetaMinus] = -s;
	scal[offScal] = t;
}

