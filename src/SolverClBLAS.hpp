/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef SOLVERCLBLAS_HPP
#define SOLVERCLBLAS_HPP

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <list>
#include <vector>
#include <CL/cl.h>

#include "OpenClEnvironment.hpp"
#include "OpenClInterface.hpp"
#include "Solver.hpp"

template<typename T>
class SolverClBLAS : public Solver<T>
{
private:
	int n;
	int numOfQueues;
	OpenClEnvironment* openClEnvironment;
	OpenClInterface<T>* openClInterface;
	std::vector<cl_command_queue>* queues;
	T* norms;
	T* dots;
	T* scals;
	T* alphas;
	std::vector<cl_mem>* scratches;

	std::list<cl_event>* events;

public:
	SolverClBLAS(const int n, const int numOfQueues, OpenClEnvironment* openClEnvironment);
	virtual ~SolverClBLAS();

	/*
	 * Helper functions for setting and getting data
	 */
	virtual void getVector(int n, cl_mem device, size_t offDevice, int incDevice, T* host, int incHost, int queueID = -1);
	virtual void setVector(int n, T* host, int incHost, cl_mem device, size_t offDevice, int incDevice, int queueID = -1);

	/*
	 * BLAS level 1 routines
	 */
	// vector addition
	virtual void axpy(int n, cl_mem alpha, size_t offAlpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, int queueID = -1);
	// copy
	virtual void copy(int n, cl_mem from, size_t offFrom, int incFrom, cl_mem to, size_t offTo, int incTo, int queueID = -1);
	// dot product
	virtual void dot(int n, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem result, size_t offResult, int queueID = -1);
	// euclidean norm
	virtual void nrm2(int n, cl_mem x, size_t offX, int incX, cl_mem result, size_t offResult, int queueID = -1);
	// scalar-vector multiplication
	virtual void scal(int n, cl_mem alpha, size_t offAlpha, cl_mem x, size_t offX, int incX, int queueID = -1);

	/*
	 * BLAS level 2 routines
	 */
	// general matrix-vector product
	virtual void gemv(int m, int n, cl_mem alpha, cl_mem A, size_t offA, int lda, cl_mem x, size_t offX, int incX, cl_mem beta, cl_mem y, size_t offY, int incY, MatrixOperation matrixOperation = NORMAL, int queueID = -1);
	// general rank-1 update
	virtual void ger(int m, int n, cl_mem alpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem A, size_t offA, int lda, int queueID = -1);
	// symmetric matrix-vector product
	virtual void symv(int n, cl_mem alpha, cl_mem A, size_t offA, int lda, cl_mem x, size_t offX, int incX, cl_mem beta, cl_mem y, size_t offY, int incY, int queueID = -1);
	// symmetric rank-2 update
	virtual void syr2(int n, cl_mem alpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem A, size_t offA, int lda, int queueID = -1);

	/*
	 * Miscellaneous functions which are required for the SBTH algorithm but
	 * which can't be expressed by BLAS routines.
	 */
	virtual void determineHouseholderScal(cl_mem x0, size_t offX0, cl_mem norm, size_t offNorm, cl_mem betaMinus, size_t offBetaMinus, cl_mem scal, size_t offScal, int queueID = -1);
	virtual void determineUnitScal(cl_mem norm, size_t offNorm, cl_mem scal, size_t offScal, int queueID = -1);

	/*
	 *
	 */
	virtual void synchronizeDevice();

	/*
	 *
	 */
	virtual T getSize();
};

#endif
