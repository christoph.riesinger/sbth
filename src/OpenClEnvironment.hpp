/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef OPENCLENVIRONMENT_HPP
#define OPENCLENVIRONMENT_HPP

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <CL/cl.h>

class OpenClEnvironment
{
private:
	cl_platform_id platform;
	cl_device_id device;
	cl_context context;

public:
	OpenClEnvironment(cl_device_type deviceType);
	~OpenClEnvironment();

	cl_device_id* getDevice();
	cl_context* getContext();
};

#endif
