/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef MATRIX_HPP
#define MATRIX_HPP

#include <cstddef>
#ifdef OPENCL
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <CL/cl.h>

#include "OpenClEnvironment.hpp"
#endif

template<typename T>
class Matrix
{
private:
#ifdef OPENCL
	cl_mem A;
#else
	T* A;
#endif
	int n;
	int d;
#ifdef OPENCL
	OpenClEnvironment* openClEnvironment;
	cl_command_queue queue;
#endif

public:
#ifdef OPENCL
	Matrix(const int n, const int d, OpenClEnvironment* openClEnvironment);
#else
	Matrix(const int n, const int d);
#endif
	~Matrix();

#ifdef OPENCL
	cl_mem getA();
#else
	T* getA();
#endif
	int getN();
	int getD();
	size_t getOffset(int row, int col);
	T getSize();
	void makeI();
	void makeRandom();
	void makeToeplitz();
	void toConsole();
	void toCSV();
	void toDat();
};

#endif
