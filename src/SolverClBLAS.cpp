/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "SolverClBLAS.hpp"

#include <cassert>
#include <clBLAS.h>

#include "common.h"
#include "Matrix.hpp"

template <class T>
SolverClBLAS<T>::SolverClBLAS(const int n, const int numOfQueues, OpenClEnvironment* openClEnvironment) :
		n(n), numOfQueues(numOfQueues), openClEnvironment(openClEnvironment)
{
	cl_int error;

	CLBLAS_ERROR_CHECK(clblasSetup());

	openClInterface = new OpenClInterface<T>(this->openClEnvironment);
	queues = new std::vector<cl_command_queue>(this->numOfQueues);

	for (int i = 0; i < this->numOfQueues; i++)
	{
		queues->at(i) = clCreateCommandQueue(*openClEnvironment->getContext(), *openClEnvironment->getDevice(), 0, &error);
		OPENCL_ERROR_CHECK(error)
	}

	dots = new T[this->numOfQueues];
	norms = new T[this->numOfQueues];
	scals = new T[this->numOfQueues];
	alphas = new T[this->numOfQueues];

	scratches = new std::vector<cl_mem>(this->numOfQueues);

	for (int i = 0; i < this->numOfQueues; i++)
	{
		scratches->at(i) = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, 2 * this->n * sizeof(T), NULL, &error);
		OPENCL_ERROR_CHECK(error)
	}

	events = new std::list<cl_event>();
}

template <class T>
SolverClBLAS<T>::~SolverClBLAS()
{
	/*
	for (std::list<cl_event>::iterator iterator = events->end(), begin = events->begin(); iterator != begin; --iterator)
		OPENCL_ERROR_CHECK(clReleaseEvent(*iterator))
	*/

	delete events;

	for (int i = numOfQueues - 1; i >= 0; i--)
		OPENCL_ERROR_CHECK(clReleaseMemObject(scratches->at(i)))

	delete scratches;

	delete alphas;
	delete scals;
	delete norms;
	delete dots;

	for (int i = numOfQueues - 1; i >= 0; i--)
		OPENCL_ERROR_CHECK(clReleaseCommandQueue(queues->at(i)))

	delete queues;
	delete openClInterface;

	clblasTeardown();
}

template <class T>
void SolverClBLAS<T>::getVector(int n, cl_mem device, size_t offDevice, int incDevice, T* host, int incHost, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasReadVectorAsync(
			n,
			sizeof(T),
			device,
			offDevice,
			host,
			0,
			queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <class T>
void SolverClBLAS<T>::setVector(int n, T* host, int incHost, cl_mem device, size_t offDevice, int incDevice, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasWriteVectorAsync(
			n,
			sizeof(T),
			host,
			0,
			device,
			offDevice,
			queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <>
void SolverClBLAS<float>::axpy(int n, cl_mem alpha, size_t offAlpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasReadVectorAsync(
			1,
			sizeof(float),
			alpha,
			offAlpha,
			&alphas[queueID],
			0,
			queues->at(queueID),
			0,
			NULL,
			NULL))
	CLBLAS_ERROR_CHECK(clblasSaxpy(
			n,
			alphas[queueID],
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <>
void SolverClBLAS<double>::axpy(int n, cl_mem alpha, size_t offAlpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasReadVectorAsync(
			1,
			sizeof(double),
			alpha,
			offAlpha,
			&alphas[queueID],
			0,
			queues->at(queueID),
			0,
			NULL,
			NULL))
	CLBLAS_ERROR_CHECK(clblasDaxpy(
			n,
			alphas[queueID],
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <>
void SolverClBLAS<float>::copy(int n, cl_mem from, size_t offFrom, int incFrom, cl_mem to, size_t offTo, int incTo, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	if (n > 0)
	{
		if (incFrom)
		{
			CLBLAS_ERROR_CHECK(clblasScopy(
					n,
					from,
					offFrom,
					incFrom,
					to,
					offTo,
					incTo,
					1,
					&queues->at(queueID),
					0,
					NULL,
					NULL))
		} else {
			const float zero = 0;

			CLBLAS_ERROR_CHECK(clblasFillVector(
					n,
					sizeof(float),
					to,
					offTo,
					&zero,
					queues->at(queueID),
					0,
					NULL))
		}
	}
}

template <>
void SolverClBLAS<double>::copy(int n, cl_mem from, size_t offFrom, int incFrom, cl_mem to, size_t offTo, int incTo, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	if (n > 0)
	{
		if (incFrom)
		{
			CLBLAS_ERROR_CHECK(clblasDcopy(
					n,
					from,
					offFrom,
					incFrom,
					to,
					offTo,
					incTo,
					1,
					&queues->at(queueID),
					0,
					NULL,
					NULL))
		} else {
			const double zero = 0;

			CLBLAS_ERROR_CHECK(clblasFillVector(
					n,
					sizeof(double),
					to,
					offTo,
					&zero,
					queues->at(queueID),
					0,
					NULL))
		}
	}
}

template <>
void SolverClBLAS<float>::dot(int n, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasSdot(
			n,
			result,
			offResult,
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			scratches->at(queueID),
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <>
void SolverClBLAS<double>::dot(int n, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasDdot(
			n,
			result,
			offResult,
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			scratches->at(queueID),
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <>
void SolverClBLAS<float>::nrm2(int n, cl_mem x, size_t offX, int incX, cl_mem result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasSnrm2(
			n,
			result,
			offResult,
			x,
			offX,
			incX,
			scratches->at(queueID),
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <>
void SolverClBLAS<double>::nrm2(int n, cl_mem x, size_t offX, int incX, cl_mem result, size_t offResult, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasDnrm2(
			n,
			result,
			offResult,
			x,
			offX,
			incX,
			scratches->at(queueID),
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
}

template <>
void SolverClBLAS<float>::scal(int n, cl_mem alpha, size_t offAlpha, cl_mem x, size_t offX, int incX, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	if (n)
	{
		CLBLAS_ERROR_CHECK(clblasReadVectorAsync(
				1,
				sizeof(float),
				alpha,
				offAlpha,
				&alphas[queueID],
				0,
				queues->at(queueID),
				0,
				NULL,
				NULL))
		CLBLAS_ERROR_CHECK(clblasSscal(
				n,
				alphas[queueID],
				x,
				offX,
				incX,
				1,
				&queues->at(queueID),
				0,
				NULL,
				NULL))
	}
}

template <>
void SolverClBLAS<double>::scal(int n, cl_mem alpha, size_t offAlpha, cl_mem x, size_t offX, int incX, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	if (n)
	{
		CLBLAS_ERROR_CHECK(clblasReadVectorAsync(
				1,
				sizeof(double),
				alpha,
				offAlpha,
				&alphas[queueID],
				0,
				queues->at(queueID),
				0,
				NULL,
				NULL))
		CLBLAS_ERROR_CHECK(clblasDscal(
				n,
				alphas[queueID],
				x,
				offX,
				incX,
				1,
				&queues->at(queueID),
				0,
				NULL,
				NULL))
	}
}

template <>
void SolverClBLAS<float>::gemv(int m, int n, cl_mem alpha, cl_mem A, size_t offA, int lda, cl_mem x, size_t offX, int incX, cl_mem beta, cl_mem y, size_t offY, int incY, MatrixOperation matrixOperation, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	clblasTranspose clTranspose;

	switch (matrixOperation) {
		case NORMAL:
			clTranspose = clblasNoTrans;
			break;
		case TRANSPOSE:
			clTranspose = clblasTrans;
			break;
		default:
			clTranspose = clblasNoTrans;
			break;
	}

	CLBLAS_ERROR_CHECK(clblasSgemv(
			clblasColumnMajor,
			clTranspose,
			m,
			n,
			1,
			A,
			offA,
			lda,
			x,
			offX,
			incX,
			0,
			y,
			offY,
			incY,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, x, offX, &queues->at(queueID));
}

template <>
void SolverClBLAS<double>::gemv(int m, int n, cl_mem alpha, cl_mem A, size_t offA, int lda, cl_mem x, size_t offX, int incX, cl_mem beta, cl_mem y, size_t offY, int incY, MatrixOperation matrixOperation, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	clblasTranspose clTranspose;

	switch (matrixOperation) {
		case NORMAL:
			clTranspose = clblasNoTrans;
			break;
		case TRANSPOSE:
			clTranspose = clblasTrans;
			break;
		default:
			clTranspose = clblasNoTrans;
			break;
	}

	CLBLAS_ERROR_CHECK(clblasDgemv(
			clblasColumnMajor,
			clTranspose,
			m,
			n,
			1,
			A,
			offA,
			lda,
			x,
			offX,
			incX,
			0,
			y,
			offY,
			incY,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, x, offX, &queues->at(queueID));
}

template <>
void SolverClBLAS<float>::ger(int m, int n, cl_mem alpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasSger(
			clblasColumnMajor,
			m,
			n,
			-1,
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			A,
			offA,
			lda,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, x, offX, &queues->at(queueID));
}

template <>
void SolverClBLAS<double>::ger(int m, int n, cl_mem alpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasDger(
			clblasColumnMajor,
			m,
			n,
			-1,
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			A,
			offA,
			lda,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, x, offX, &queues->at(queueID));
}

template <>
void SolverClBLAS<float>::symv(int n, cl_mem alpha, cl_mem A, size_t offA, int lda, cl_mem x, size_t offX, int incX, cl_mem beta, cl_mem y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasSsymv(
			clblasColumnMajor,
			clblasLower,
			n,
			1,
			A,
			offA,
			lda,
			x,
			offX,
			incX,
			0,
			y,
			offY,
			incY,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, x, offX, &queues->at(queueID));
}

template <>
void SolverClBLAS<double>::symv(int n, cl_mem alpha, cl_mem A, size_t offA, int lda, cl_mem x, size_t offX, int incX, cl_mem beta, cl_mem y, size_t offY, int incY, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	CLBLAS_ERROR_CHECK(clblasDsymv(
			clblasColumnMajor,
			clblasLower,
			n,
			1,
			A,
			offA,
			lda,
			x,
			offX,
			incX,
			0,
			y,
			offY,
			incY,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, x, offX, &queues->at(queueID));
}

template <>
void SolverClBLAS<float>::syr2(int n, cl_mem alpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	/*
	 * TODO
	 * clblasSsyr2() crashes for alpha != 0 and n > 35. It can be avoided if
	 * lines 289 and 372 are deleted from syr2_her2.cl.
	 */
	// const cl_float minus = -1;
	const cl_float minus = -1;

	CLBLAS_ERROR_CHECK(clblasSsyr2(
			clblasColumnMajor,
			clblasLower,
			n,
			minus,
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			A,
			offA,
			lda,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, y, offY, &queues->at(queueID));
}

template <>
void SolverClBLAS<double>::syr2(int n, cl_mem alpha, cl_mem x, size_t offX, int incX, cl_mem y, size_t offY, int incY, cl_mem A, size_t offA, int lda, int queueID)
{
	assert(queueID >= 0);
	assert(queueID < numOfQueues);

	/*
	 * TODO
	 * clblasSsyr2() crashes for alpha != 0 and n > 33. It can be avoided if
	 * lines 289 and 372 are deleted from syr2_her2.cl.
	 */
	// const cl_double minus = -1;
	const cl_double minus = -1;

	CLBLAS_ERROR_CHECK(clblasDsyr2(
			clblasColumnMajor,
			clblasLower,
			n,
			minus,
			x,
			offX,
			incX,
			y,
			offY,
			incY,
			A,
			offA,
			lda,
			1,
			&queues->at(queueID),
			0,
			NULL,
			NULL))
	
	openClInterface->computeDummy(n, y, offY, &queues->at(queueID));
}

template <class T>
void SolverClBLAS<T>::determineHouseholderScal(cl_mem x0, size_t offX0, cl_mem norm, size_t offNorm, cl_mem betaMinus, size_t offBetaMinus, cl_mem scal, size_t offScal, int queueID)
{
	assert(queueID < numOfQueues);

	if (queueID >= 0)
		openClInterface->determineHouseholderScal(x0, offX0, norm, offNorm, betaMinus, offBetaMinus, scal, offScal, &queues->at(queueID));
	else
		openClInterface->determineHouseholderScal(x0, offX0, norm, offNorm, betaMinus, offBetaMinus, scal, offScal);
}

template <class T>
void SolverClBLAS<T>::determineUnitScal(cl_mem norm, size_t offNorm, cl_mem scal, size_t offScal, int queueID)
{
	assert(queueID < numOfQueues);

	if (queueID >= 0)
		openClInterface->determineUnitScal(norm, offNorm, scal, offScal, &queues->at(queueID));
	else
		openClInterface->determineUnitScal(norm, offNorm, scal, offScal);
}

template <class T>
void SolverClBLAS<T>::synchronizeDevice()
{
	for (int i = 0; i < numOfQueues; i++)
		OPENCL_ERROR_CHECK(clFinish(queues->at(i)))
}

template <class T>
T SolverClBLAS<T>::getSize()
{
	T size;

	size = (T)numOfQueues * (T)2 * (T)n * (T)sizeof(T);

	return size;
}

template class SolverClBLAS<float>;
template class SolverClBLAS<double>;
