/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef OPENCLINTERFACE_CUH
#define OPENCLINTERFACE_CUH

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <CL/cl.h>

#include "OpenClEnvironment.hpp"

template<typename T>
class OpenClInterface
{
private:
	OpenClEnvironment* openClEnvironment;
	cl_program determineHouseholderScalProgram;
	cl_program determineUnitScalProgram;
	cl_program computeDummyProgram;
	cl_kernel determineHouseholderScalKernel;
	cl_kernel determineUnitScalKernel;
	cl_kernel computeDummyKernel;

public:
	OpenClInterface(OpenClEnvironment* openClEnvironment);
	virtual ~OpenClInterface();

	void determineHouseholderScal(cl_mem x0, size_t offX0, cl_mem norm, size_t offNorm, cl_mem betaMinus, size_t offBetaMinus, cl_mem scal, size_t offScal, cl_command_queue* queue = NULL);
	void determineUnitScal(cl_mem norm, size_t offNorm, cl_mem scal, size_t offScal, cl_command_queue* queue = NULL);
	void computeDummy(const size_t n, cl_mem data, size_t offData, cl_command_queue* queue = NULL);
};

#endif
