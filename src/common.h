/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef COMMON_H
#define COMMON_H

#include <cstdlib>
#include <iostream>
#ifdef CUDA
#include <cuda_runtime.h>
#include <cublas_v2.h>
#endif
#ifdef OPENCL
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <CL/cl.h>

#include <clBLAS.h>
#endif

#include "constants.h"

enum MatrixOperation
{
	NORMAL,
	TRANSPOSE
};

#ifdef CUDA
#define CUDA_ERROR_CHECK(code) \
{ \
	cudaErrorCheck((code), __FILE__, __LINE__); \
}
#endif

#ifdef CUDA
#define CUBLAS_ERROR_CHECK(code) \
{ \
	cublasErrorCheck((code), __FILE__, __LINE__); \
}
#endif

#ifdef OPENCL
#define OPENCL_ERROR_CHECK(code) \
{ \
	openclErrorCheck((code), __FILE__, __LINE__); \
}
#endif

#ifdef OPENCL
#define CLBLAS_ERROR_CHECK(code) \
{ \
	clblasErrorCheck((code), __FILE__, __LINE__); \
}
#endif

#ifdef CUDA
inline std::string cublasGetErrorString(cublasStatus_t code)
{
	switch (code)
	{
		case CUBLAS_STATUS_SUCCESS:
			return "CUBLAS_STATUS_SUCCESS";
		case CUBLAS_STATUS_NOT_INITIALIZED:
			return "CUBLAS_STATUS_NOT_INITIALIZED";
		case CUBLAS_STATUS_ALLOC_FAILED:
			return "CUBLAS_STATUS_ALLOC_FAILED";
		case CUBLAS_STATUS_INVALID_VALUE:
			return "CUBLAS_STATUS_INVALID_VALUE";
		case CUBLAS_STATUS_ARCH_MISMATCH:
			return "CUBLAS_STATUS_ARCH_MISMATCH";
		case CUBLAS_STATUS_MAPPING_ERROR:
			return "CUBLAS_STATUS_MAPPING_ERROR";
		case CUBLAS_STATUS_EXECUTION_FAILED:
			return "CUBLAS_STATUS_EXECUTION_FAILED";
		case CUBLAS_STATUS_INTERNAL_ERROR:
			return "CUBLAS_STATUS_INTERNAL_ERROR";
		case CUBLAS_STATUS_NOT_SUPPORTED:
			return "CUBLAS_STATUS_NOT_SUPPORTED";
		case CUBLAS_STATUS_LICENSE_ERROR:
			return "CUBLAS_STATUS_LICENSE_ERROR";
		default:
			return "CUBLAS_STATUS_UNKNOWN_ERROR";
	}
}
#endif

#ifdef OPENCL
inline std::string openclGetErrorString(cl_int code)
{
	switch (code)
	{
		case 0:
			return "CL_SUCCESS";
		case -1:
			return "CL_DEVICE_NOT_FOUND";
		case -2:
			return "CL_DEVICE_NOT_AVAILABLE";
		case -3:
			return "CL_COMPILER_NOT_AVAILABLE";
		case -4:
			return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
		case -5:
			return "CL_OUT_OF_RESOURCES";
		case -6:
			return "CL_OUT_OF_HOST_MEMORY";
		case -7:
			return "CL_PROFILING_INFO_NOT_AVAILABLE";
		case -8:
			return "CL_MEM_COPY_OVERLAP";
		case -9:
			return "CL_IMAGE_FORMAT_MISMATCH";
		case -10:
			return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
		case -11:
			return "CL_BUILD_PROGRAM_FAILURE";
		case -12:
			return "CL_MAP_FAILURE";
		case -13:
			return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
		case -14:
			return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
		case -15:
			return "CL_COMPILE_PROGRAM_FAILURE";
		case -16:
			return "CL_LINKER_NOT_AVAILABLE";
		case -17:
			return "CL_LINK_PROGRAM_FAILURE";
		case -18:
			return "CL_DEVICE_PARTITION_FAILED";
		case -19:
			return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
		case -30:
			return "CL_INVALID_VALUE";
		case -31:
			return "CL_INVALID_DEVICE_TYPE";
		case -32:
			return "CL_INVALID_PLATFORM";
		case -33:
			return "CL_INVALID_DEVICE";
		case -34:
			return "CL_INVALID_CONTEXT";
		case -35:
			return "CL_INVALID_QUEUE_PROPERTIES";
		case -36:
			return "CL_INVALID_COMMAND_QUEUE";
		case -37:
			return "CL_INVALID_HOST_PTR";
		case -38:
			return "CL_INVALID_MEM_OBJECT";
		case -39:
			return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
		case -40:
			return "CL_INVALID_IMAGE_SIZE";
		case -41:
			return "CL_INVALID_SAMPLER";
		case -42:
			return "CL_INVALID_BINARY";
		case -43:
			return "CL_INVALID_BUILD_OPTIONS";
		case -44:
			return "CL_INVALID_PROGRAM";
		case -45:
			return "CL_INVALID_PROGRAM_EXECUTABLE";
		case -46:
			return "CL_INVALID_KERNEL_NAME";
		case -47:
			return "CL_INVALID_KERNEL_DEFINITION";
		case -48:
			return "CL_INVALID_KERNEL";
		case -49:
			return "CL_INVALID_ARG_INDEX";
		case -50:
			return "CL_INVALID_ARG_VALUE";
		case -51:
			return "CL_INVALID_ARG_SIZE";
		case -52:
			return "CL_INVALID_KERNEL_ARGS";
		case -53:
			return "CL_INVALID_WORK_DIMENSION";
		case -54:
			return "CL_INVALID_WORK_GROUP_SIZE";
		case -55:
			return "CL_INVALID_WORK_ITEM_SIZE";
		case -56:
			return "CL_INVALID_GLOBAL_OFFSET";
		case -57:
			return "CL_INVALID_EVENT_WAIT_LIST";
		case -58:
			return "CL_INVALID_EVENT";
		case -59:
			return "CL_INVALID_OPERATION";
		case -60:
			return "CL_INVALID_GL_OBJECT";
		case -61:
			return "CL_INVALID_BUFFER_SIZE";
		case -62:
			return "CL_INVALID_MIP_LEVEL";
		case -63:
			return "CL_INVALID_GLOBAL_WORK_SIZE";
		case -64:
			return "CL_INVALID_PROPERTY";
		case -65:
			return "CL_INVALID_IMAGE_DESCRIPTOR";
		case -66:
			return "CL_INVALID_COMPILER_OPTIONS";
		case -67:
			return "CL_INVALID_LINKER_OPTIONS";
		case -68:
			return "CL_INVALID_DEVICE_PARTITION_COUNT";
		case -1000:
			return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
		case -1001:
			return "CL_PLATFORM_NOT_FOUND_KHR";
		case -1002:
			return "CL_INVALID_D3D10_DEVICE_KHR";
		case -1003:
			return "CL_INVALID_D3D10_RESOURCE_KHR";
		case -1004:
			return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
		case -1005:
			return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
		case -9999:
			return "Illegal read or write to a buffer";
		default:
			return "Unknown OpenCL error";
	}
}
#endif

#ifdef OPENCL
inline std::string clblasGetErrorString(clblasStatus code)
{
	switch (code)
	{
		case clblasSuccess:
			return "clblasSuccess";
		case clblasInvalidValue:
			return "clblasInvalidValue";
		case clblasInvalidCommandQueue:
			return "clblasInvalidCommandQueue";
		case clblasInvalidContext:
			return "clblasInvalidContext";
		case clblasInvalidMemObject:
			return "clblasInvalidMemObject";
		case clblasInvalidDevice:
			return "clblasInvalidDevice";
		case clblasInvalidEventWaitList:
			return "clblasInvalidEventWaitList";
		case clblasOutOfResources:
			return "clblasOutOfResources";
		case clblasOutOfHostMemory:
			return "clblasOutOfHostMemory";
		case clblasInvalidOperation:
			return "clblasInvalidOperation";
		case clblasCompilerNotAvailable:
			return "clblasCompilerNotAvailable";
		case clblasBuildProgramFailure:
			return "clblasBuildProgramFailure";
		// Extended error codes
		case clblasNotImplemented:
			return "Functionality is not implemented";
		case clblasNotInitialized:
			return "clblas library is not initialized yet";
		case clblasInvalidMatA:
			return "Matrix A is not a valid memory object";
		case clblasInvalidMatB:
			return "Matrix B is not a valid memory object";
		case clblasInvalidMatC:
			return "Matrix C is not a valid memory object";
		case clblasInvalidVecX:
			return "Vector X is not a valid memory object";
		case clblasInvalidVecY:
			return "Vector Y is not a valid memory object";
		case clblasInvalidDim:
			return "An input dimension (M,N,K) is invalid";
		case clblasInvalidLeadDimA:
			return "Leading dimension A must not be less than the size of the first dimension";
		case clblasInvalidLeadDimB:
			return "Leading dimension B must not be less than the size of the first dimension";
		case clblasInvalidLeadDimC:
			return "Leading dimension C must not be less than the size of the first dimension";
		case clblasInvalidIncX:
			return "The increment for a vector X must not be 0";
		case clblasInvalidIncY:
			return "The increment for a vector Y must not be 0";
		case clblasInsufficientMemMatA:
			return "The memory object for Matrix A is too small";
		case clblasInsufficientMemMatB:
			return "The memory object for Matrix B is too small";
		case clblasInsufficientMemMatC:
			return "The memory object for Matrix C is too small";
		case clblasInsufficientMemVecX:
			return "The memory object for Vector X is too small";
		case clblasInsufficientMemVecY:
			return "The memory object for Vector Y is too small";
		default:
			return "Unknown clBLAS error";
	}
}
#endif

#ifdef CUDA
inline void cudaErrorCheck(cudaError_t code, std::string file, int line, bool abort = true)
{
	if (code != cudaSuccess)
	{
		std::cerr << "----- !!! The following CUDA error occurred !!! -----" << std::endl;
		std::cerr << "error code:	 " << code << std::endl;
		std::cerr << "error string:   " << cudaGetErrorString(code) << std::endl;
		std::cerr << "error location: " << file << ":" << line << std::endl;
		std::cerr << "-----------------------------------------------------" << std::endl;

		if(abort)
			exit (code);
	}
}
#endif

#ifdef CUDA
inline void cublasErrorCheck(cublasStatus_t code, std::string file, int line, bool abort = true)
{
	if (code != CUBLAS_STATUS_SUCCESS)
	{
		std::cerr << "----- !!! The following cuBLAS error occurred !!! -----" << std::endl;
		std::cerr << "error code:	 " << code << std::endl;
		std::cerr << "error string:   " << cublasGetErrorString(code) << std::endl;
		std::cerr << "error location: " << file << ":" << line << std::endl;
		std::cerr << "-------------------------------------------------------" << std::endl;

		if(abort)
			exit (code);
	}
}
#endif

#ifdef OPENCL
inline void openclErrorCheck(cl_int code, std::string file, int line, bool abort = true)
{
	if (code != CL_SUCCESS)
	{
		std::cerr << "----- !!! The following OpenCL error occurred !!! -----" << std::endl;
		std::cerr << "error code:	 " << code << std::endl;
		std::cerr << "error string:   " << openclGetErrorString(code) << std::endl;
		std::cerr << "error location: " << file << ":" << line << std::endl;
		std::cerr << "-------------------------------------------------------" << std::endl;

		if(abort)
			exit (code);
	}
}
#endif

#ifdef OPENCL
inline void clblasErrorCheck(clblasStatus code, std::string file, int line, bool abort = true)
{
	if (code != CL_SUCCESS)
	{
		std::cerr << "----- !!! The following clBLAS error occurred !!! -----" << std::endl;
		std::cerr << "error code:	 " << code << std::endl;
		std::cerr << "error string:   " << clblasGetErrorString(code) << std::endl;
		std::cerr << "error location: " << file << ":" << line << std::endl;
		std::cerr << "-------------------------------------------------------" << std::endl;

		if(abort)
			exit (code);
	}
}
#endif

template <class T>
inline const char* typeName()
{
	std::cerr << "----- typeName() -----" << std::endl;
	std::cerr << "Unknown type name." << std::endl;
	std::cerr << "----------------------" << std::endl;

	exit (EXIT_FAILURE);
}

template <>
inline const char* typeName<int>()
{
	return "I";
}

template <>
inline const char* typeName<float>()
{
	return "S";
}

template <>
inline const char* typeName<double>()
{
	return "D";
}

template <class T>
inline int sgn(T x)
{
	return ((T(0) < x) - (x < T(0)));
}

#endif
