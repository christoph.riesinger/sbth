/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#ifndef RUNNER_HPP
#define RUNNER_HPP

#ifdef OPENCL
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS
#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <CL/cl.h>

#include "OpenClEnvironment.hpp"
#endif

#include "Matrix.hpp"
#include "Solver.hpp"

template<typename T>
class Runner
{
private:
	Matrix<T>* matrix;
	Solver<T>* solver;
	int pipelineLength;
#ifdef OPENCL
	OpenClEnvironment* openClEnvironment;
	cl_mem vMain;
	cl_mem vSub;
	cl_mem xMain;
	cl_mem xSub;
	cl_mem h;
	cl_mem zSub;
	cl_mem wMain;
	cl_mem wSub;
	cl_mem norm;
	cl_mem dot;
	cl_mem ZERO;
	cl_mem HALF;
	cl_mem ONE;
	cl_mem MINUS;
#else
	T* vMain;
	T* vSub;
	T* xMain;
	T* xSub;
	T* h;
	T* zSub;
	T* wMain;
	T* wSub;
	T* norm;
	T* dot;
	T* ZERO;
	T* HALF;
	T* ONE;
	T* MINUS;
#endif

	inline int computeB(const int nu);
	inline int computeR(const int nu, const int b);
	inline int computeIdx(const int nu, const int beta);
	void processBlockPairs(const int step);

public:
#ifdef OPENCL
	Runner(Matrix<T>* matrix, Solver<T>* solver, int pipelineLength, OpenClEnvironment* openClEnvironment);
#else
	Runner(Matrix<T>* matrix, Solver<T>* solver, int pipelineLength);
#endif
	~Runner();

	T getSize();
	void run();
};

#endif
