/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "OpenClInterface.hpp"

#include <fstream>
#include <sstream>

#include "common.h"

template <class T>
OpenClInterface<T>::OpenClInterface(OpenClEnvironment* openClEnvironment) :
		openClEnvironment(openClEnvironment)
{
	const char options[] = "-Werror";
	cl_int error;
	std::ifstream file;
	size_t determineHouseholderScalSize;
	size_t determineUnitScalSize;
	size_t computeDummySize;
	char* determineHouseholderScalCode;
	char* determineUnitScalCode;
	char* computeDummyCode;

	file.open("src/determineHouseholderScal.cl", std::ios::in);
	if (file.is_open())
	{
		file.seekg(0, std::ios::end);
		determineHouseholderScalSize = file.tellg();
		determineHouseholderScalCode = new char[determineHouseholderScalSize];
		file.seekg(0, std::ios::beg);
		file.read(determineHouseholderScalCode, determineHouseholderScalSize);
		file.close();
	} else {
		std::cerr << "----- OpenClInterface<T>::OpenClInterface -----" << std::endl;
		std::cerr << "Could not open kernel file determineHouseholderScalKernel.cl." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "-----------------------------------------------" << std::endl;

		exit (EXIT_FAILURE);
	}
	file.open("src/determineUnitScal.cl", std::ios::in);
	if (file.is_open())
	{
		file.seekg(0, std::ios::end);
		determineUnitScalSize = file.tellg();
		determineUnitScalCode = new char[determineUnitScalSize];
		file.seekg(0, std::ios::beg);
		file.read(determineUnitScalCode, determineUnitScalSize);
		file.close();
	} else {
		std::cerr << "----- OpenClInterface<T>::OpenClInterface -----" << std::endl;
		std::cerr << "Could not open kernel file determineUnitScal.cl." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "-----------------------------------------------" << std::endl;

		exit (EXIT_FAILURE);
	}
	file.open("src/computeDummy.cl", std::ios::in);
	if (file.is_open())
	{
		file.seekg(0, std::ios::end);
		computeDummySize = file.tellg();
		computeDummyCode = new char[computeDummySize];
		file.seekg(0, std::ios::beg);
		file.read(computeDummyCode, computeDummySize);
		file.close();
	} else {
		std::cerr << "----- OpenClInterface<T>::OpenClInterface -----" << std::endl;
		std::cerr << "Could not open kernel file computeDummy.cl." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "-----------------------------------------------" << std::endl;

		exit (EXIT_FAILURE);
	}

	std::stringstream determineHouseholderScalKernelName;
	determineHouseholderScalKernelName << "determineHouseholderScal" << typeName<T>() << "Kernel";
	std::stringstream determineUnitScalKernelName;
	determineUnitScalKernelName << "determineUnitScal" << typeName<T>() << "Kernel";
	std::stringstream computeDummyKernelName;
	computeDummyKernelName << "computeDummy" << typeName<T>() << "Kernel";

	determineHouseholderScalProgram = clCreateProgramWithSource(
			*openClEnvironment->getContext(),
		 	1,
		 	(const char**) &determineHouseholderScalCode,
		 	&determineHouseholderScalSize,
		 	&error);
	OPENCL_ERROR_CHECK(error)
	OPENCL_ERROR_CHECK(clBuildProgram(
			determineHouseholderScalProgram,
			1,
			openClEnvironment->getDevice(),
			options,
			NULL,
			NULL))
	determineHouseholderScalKernel = clCreateKernel(
			determineHouseholderScalProgram,
			determineHouseholderScalKernelName.str().c_str(),
			&error);
	OPENCL_ERROR_CHECK(error)
	determineUnitScalProgram = clCreateProgramWithSource(
			*openClEnvironment->getContext(),
			1,
			(const char**) &determineUnitScalCode,
			&determineUnitScalSize,
			&error);
	OPENCL_ERROR_CHECK(error)
	OPENCL_ERROR_CHECK(clBuildProgram(
			determineUnitScalProgram,
			1,
			openClEnvironment->getDevice(),
			options,
			NULL,
			NULL))
	determineUnitScalKernel = clCreateKernel(
			determineUnitScalProgram,
			determineUnitScalKernelName.str().c_str(),
			&error);
	OPENCL_ERROR_CHECK(error)
	computeDummyProgram = clCreateProgramWithSource(
			*openClEnvironment->getContext(),
		 	1,
		 	(const char**) &computeDummyCode,
		 	&computeDummySize,
		 	&error);
	OPENCL_ERROR_CHECK(error)
	OPENCL_ERROR_CHECK(clBuildProgram(
			computeDummyProgram,
			1,
			openClEnvironment->getDevice(),
			options,
			NULL,
			NULL))
	computeDummyKernel = clCreateKernel(
			computeDummyProgram,
			computeDummyKernelName.str().c_str(),
			&error);
	OPENCL_ERROR_CHECK(error)

	delete[] determineHouseholderScalCode;
	delete[] determineUnitScalCode;
	delete[] computeDummyCode;

	/*
	cl_build_status status;
	char* message;
	size_t messageLength;

	OPENCL_ERROR_CHECK(clGetProgramBuildInfo(
			determineUnitScalProgram,
			*openClEnvironment->getDevice(),
			CL_PROGRAM_BUILD_STATUS,
			sizeof(cl_build_status),
			&status,
			NULL))
	OPENCL_ERROR_CHECK(clGetProgramBuildInfo(
			determineUnitScalProgram,
			*openClEnvironment->getDevice(),
			CL_PROGRAM_BUILD_LOG,
			0,
			NULL,
			&messageLength))
	message = new char[messageLength];
	OPENCL_ERROR_CHECK(clGetProgramBuildInfo(
			determineUnitScalProgram,
			*openClEnvironment->getDevice(),
			CL_PROGRAM_BUILD_LOG,
			messageLength,
			message,
			NULL))

	std::cout << status << "[" << messageLength << "]" << ": " << message << std::endl;

	delete[] message;
	*/
}

template <class T>
OpenClInterface<T>::~OpenClInterface()
{
	OPENCL_ERROR_CHECK(clReleaseKernel(computeDummyKernel))
	OPENCL_ERROR_CHECK(clReleaseKernel(computeDummyKernel))
	OPENCL_ERROR_CHECK(clReleaseKernel(determineUnitScalKernel))
	OPENCL_ERROR_CHECK(clReleaseKernel(determineHouseholderScalKernel))
	OPENCL_ERROR_CHECK(clReleaseProgram(determineUnitScalProgram))
	OPENCL_ERROR_CHECK(clReleaseProgram(determineHouseholderScalProgram))
}

template <class T>
void OpenClInterface<T>::determineHouseholderScal(cl_mem x0, size_t offX0, cl_mem norm, size_t offNorm, cl_mem betaMinus, size_t offBetaMinus, cl_mem scal, size_t offScal, cl_command_queue* queue)
{
	const size_t workGroupSize[] = {1};

	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 0, sizeof(cl_mem), (void*) &x0))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 1, sizeof(int), (void*) &offX0))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 2, sizeof(cl_mem), (void*) &norm))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 3, sizeof(int), (void*) &offNorm))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 4, sizeof(cl_mem), (void*) &betaMinus))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 5, sizeof(int), (void*) &offBetaMinus))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 6, sizeof(cl_mem), (void*) &scal))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineHouseholderScalKernel, 7, sizeof(int), (void*) &offScal))

	OPENCL_ERROR_CHECK(clEnqueueNDRangeKernel(
			*queue,
			determineHouseholderScalKernel,
		 	1,
		 	NULL,
		 	workGroupSize,
		 	workGroupSize,
		 	0,
		 	NULL,
		 	NULL))
}

template <class T>
void OpenClInterface<T>::determineUnitScal(cl_mem norm, size_t offNorm, cl_mem scal, size_t offScal, cl_command_queue* queue)
{
	const size_t workGroupSize[] = {1};

	OPENCL_ERROR_CHECK(clSetKernelArg(determineUnitScalKernel, 0, sizeof(cl_mem), (void*) &norm))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineUnitScalKernel, 1, sizeof(int), (void*) &offNorm))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineUnitScalKernel, 2, sizeof(cl_mem), (void*) &scal))
	OPENCL_ERROR_CHECK(clSetKernelArg(determineUnitScalKernel, 3, sizeof(int), (void*) &offScal))

	OPENCL_ERROR_CHECK(clEnqueueNDRangeKernel(
			*queue,
			determineUnitScalKernel,
		 	1,
		 	NULL,
		 	workGroupSize,
		 	workGroupSize,
		 	0,
		 	NULL,
		 	NULL))
}

template <class T>
void OpenClInterface<T>::computeDummy(const size_t n, cl_mem data, size_t offData, cl_command_queue* queue)
{
	/*
	const int numOfIterations = (1<<10);
	const size_t workGroupSize[] = {n, 256 / n};

	OPENCL_ERROR_CHECK(clSetKernelArg(computeDummyKernel, 0, sizeof(cl_mem), (void*) &data))
	OPENCL_ERROR_CHECK(clSetKernelArg(computeDummyKernel, 1, sizeof(int), (void*) &offData))
	OPENCL_ERROR_CHECK(clSetKernelArg(computeDummyKernel, 2, sizeof(int), (void*) &numOfIterations))

	OPENCL_ERROR_CHECK(clEnqueueNDRangeKernel(
			*queue,
			computeDummyKernel,
		 	2,
		 	NULL,
		 	workGroupSize,
		 	workGroupSize,
		 	0,
		 	NULL,
		 	NULL))
	*/
}

template class OpenClInterface<float>;
template class OpenClInterface<double>;
