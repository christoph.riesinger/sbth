/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "Matrix.hpp"

#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <typeinfo>
#ifdef CUDA
#include <cuda_runtime.h>
#endif

#include "common.h"

template <class T>
#ifdef OPENCL
Matrix<T>::Matrix(const int n, const int d, OpenClEnvironment* openClEnvironment) :
		n(n), d(d), openClEnvironment(openClEnvironment)
#else
Matrix<T>::Matrix(const int n, const int d) :
		n(n), d(d)
#endif
{
	assert(this->d > 0 && this->d < this->n);

#ifdef CUDA
	CUDA_ERROR_CHECK(cudaMalloc(&A, 2 * this->d * this->n * sizeof(T)))
#endif
#ifdef OPENCL
	cl_int error;

	A = clCreateBuffer(*openClEnvironment->getContext(), CL_MEM_READ_WRITE, 2 * this->d * this->n * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	queue = clCreateCommandQueue(*openClEnvironment->getContext(), *openClEnvironment->getDevice(), 0, &error);
	OPENCL_ERROR_CHECK(error)
#endif
}

template <class T>
Matrix<T>::~Matrix()
{
#ifdef CUDA
	CUDA_ERROR_CHECK(cudaFree(A))
#endif
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clReleaseCommandQueue(queue))
	OPENCL_ERROR_CHECK(clReleaseMemObject(A))
#endif
}

#ifdef OPENCL
template <class T>
cl_mem Matrix<T>::getA()
{
	return A;
}
#else
template <class T>
T* Matrix<T>::getA()
{
	return A;
}
#endif

template <class T>
int Matrix<T>::getN()
{
	return n;
}

template <class T>
int Matrix<T>::getD()
{
	return d;
}

template <class T>
size_t Matrix<T>::getOffset(int row, int col)
{
	assert(row >= col && row < col + 2 * d);
	assert(col < n);

	int offset;

	offset = (row - col) + (2 * d * col);

	return offset;
}

template <class T>
T Matrix<T>::getSize()
{
	T size;

	size = (T)2 * (T)d * (T)n * (T)sizeof(T);

	return size;
}

template <class T>
void Matrix<T>::makeI()
{
	T* AHost = new T[2 * d * n];

	for (int col = 0; col < n; col++)
	{
		for (int row = col; row < col + 2 * d; row++)
			AHost[getOffset(row, col)] = (abs(row - col) > d) ? (T)0 : (T)(col + 1);
	}

#ifdef CUDA
	CUDA_ERROR_CHECK(cudaMemcpy(
			A,
			AHost,
			2 * d * n * sizeof(T),
			cudaMemcpyHostToDevice))
#endif
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clEnqueueWriteBuffer(
			queue,
			A,
			CL_TRUE,
			0,
			2 * d * n * sizeof(T),
			AHost,
			0,
			NULL,
			NULL))
#endif

	delete AHost;
}

template <class T>
void Matrix<T>::makeRandom()
{
	T* AHost = new T[2 * d * n];

	for (int col = 0; col < n; col++)
		{
			for (int row = col; row < col + 2 * d; row++)
				AHost[getOffset(row, col)] = (abs(row - col) > d) ? (T)0 : ((T)rand() / (T)(RAND_MAX>>1) - (T)1);
	}

#ifdef CUDA
	CUDA_ERROR_CHECK(cudaMemcpy(
			A,
			AHost,
			2 * d * n * sizeof(T),
			cudaMemcpyHostToDevice))
#endif
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clEnqueueWriteBuffer(
			queue,
			A,
			CL_TRUE,
			0,
			2 * d * n * sizeof(T),
			AHost,
			0,
			NULL,
			NULL))
#endif

	delete AHost;
}

template <class T>
void Matrix<T>::makeToeplitz()
{
	T* AHost = new T[2 * d * n];

	for (int col = 0; col < n; col++)
	{
		for (int row = col; row < col + 2 * d; row++)
			AHost[getOffset(row, col)] = ((row - col) > d) ? (T)0 : (T)(row - col);
	}

#ifdef CUDA
	CUDA_ERROR_CHECK(cudaMemcpy(
			A,
			AHost,
			2 * d * n * sizeof(T),
			cudaMemcpyHostToDevice))
#endif
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clEnqueueWriteBuffer(
			queue,
			A,
			CL_TRUE,
			0,
			2 * d * n * sizeof(T),
			AHost,
			0,
			NULL,
			NULL))
#endif

	delete AHost;
}

template <class T>
void Matrix<T>::toConsole()
{
	T* AHost = new T[2 * d * n];

#ifdef CUDA
	CUDA_ERROR_CHECK(cudaMemcpy(
			AHost,
			A,
			2 * d * n * sizeof(T),
			cudaMemcpyDeviceToHost))
#endif
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clEnqueueReadBuffer(
			queue,
			A,
			CL_TRUE,
			0,
			2 * d * n * sizeof(T),
			AHost,
			0,
			NULL,
			NULL))
#endif

	for (int row = 0; row < n; row++)
	{
		for (int col = 0; col < n; col++)
		{
			if (row < col)
				std::cout << std::setw(12) << ((col >= row && col < row + 2 * d) ? (AHost[getOffset(col, row)]) : 0) << " ";
			else
				std::cout << std::setw(12) << ((row >= col && row < col + 2 * d) ? (AHost[getOffset(row, col)]) : 0) << " ";
		}
		std::cout << std::endl;
	}

	delete AHost;
}

template <class T>
void Matrix<T>::toCSV()
{
	std::string fileName("A.csv");
	std::ofstream file(fileName.c_str(), std::ios::out);

	if (file.is_open())
	{
		T* AHost = new T[2 * d * n];

#ifdef CUDA
	CUDA_ERROR_CHECK(cudaMemcpy(
			AHost,
			A,
			2 * d * n * sizeof(T),
			cudaMemcpyDeviceToHost))
#endif
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clEnqueueReadBuffer(
			queue,
			A,
			CL_TRUE,
			0,
			2 * d * n * sizeof(T),
			AHost,
			0,
			NULL,
			NULL))
#endif

		for (int row = 0; row < n; row++)
		{
			for (int col = 0; col < n - 1; col++)
			{
				if (row < col)
					file << ((col >= row && col < row + 2 * d) ? (AHost[getOffset(col, row)]) : 0) << ";";
				else
					file << ((row >= col && row < col + 2 * d) ? (AHost[getOffset(row, col)]) : 0) << ";";
			}
			file << ((n - 1 < row + 2 * d) ? (AHost[getOffset(n - 1, row)]) : 0) << std::endl;
		}

		delete AHost;
	} else {
		std::cerr << "----- Matrix<T>::AtoCSV() -----" << std::endl;
		std::cerr << "There is no open file to write the matrix." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "-------------------------------" << std::endl;

		exit (EXIT_FAILURE);
	}
}

template <class T>
void Matrix<T>::toDat()
{
	std::string fileName("A.dat");
	std::ofstream file(fileName.c_str(), std::ios::out);

	if (file.is_open())
	{
		T* AHost = new T[2 * d * n];

#ifdef CUDA
	CUDA_ERROR_CHECK(cudaMemcpy(
			AHost,
			A,
			2 * d * n * sizeof(T),
			cudaMemcpyDeviceToHost))
#endif
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clEnqueueReadBuffer(
			queue,
			A,
			CL_TRUE,
			0,
			2 * d * n * sizeof(T),
			AHost,
			0,
			NULL,
			NULL))
#endif

		for (int row = 0; row < n; row++)
		{
			for (int col = 0; col < n - 1; col++)
			{
				if (row < col)
					file << std::setw(12) << ((col >= row && col < row + 2 * d) ? (AHost[getOffset(col, row)]) : 0) << " ";
				else
					file << std::setw(12) << ((row >= col && row < col + 2 * d) ? (AHost[getOffset(row, col)]) : 0) << " ";
			}
			file << std::setw(12) << ((n - 1 < row + 2 * d) ? (AHost[getOffset(n - 1, row)]) : 0) << std::endl;
		}

		delete AHost;
	} else {
		std::cerr << "----- Matrix<T>::AtoDat() -----" << std::endl;
		std::cerr << "There is no open file to write the matrix." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "-------------------------------" << std::endl;

		exit (EXIT_FAILURE);
	}
}

template class Matrix<float>;
template class Matrix<double>;
