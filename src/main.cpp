/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include <cstring>
#include <iostream>
#include <unistd.h>
#include <sys/time.h>

#include "Matrix.hpp"
#include "Runner.hpp"
#include "Solver.hpp"
#ifdef CUDA
#include "SolverCuBLAS.hpp"
#include "SolverMAGMA.hpp"
#endif
#ifdef OPENCL
#include "OpenClEnvironment.hpp"
#include "SolverClBLAS.hpp"
#endif

int main(int argc, char** argv)
{
	int n = -1;
	int d = -1;
	int pipelineLength = -1;
	char* type = NULL;
	char* library = NULL;
	char optchar;
	timeval start, end;
	TYPE elapsed;

	/*
	 * Process input arguments.
	 */
	if (argc != 11)
	{
		std::cerr << "----- main() -----" << std::endl;
		std::cerr << "The following arguments have to be passed:" << std::endl;
		std::cerr << "-n: Matrix size            (int)" << std::endl;
		std::cerr << "-d: Bandwidth              (int)" << std::endl;
		std::cerr << "-p: Pipeline length        (int)" << std::endl;
		std::cerr << "-t: Matrix type            (string)" << std::endl;
		std::cerr << "-l: Linear algebra library (string)" << std::endl;
		std::cerr << "Instead, the following arguments have been passed." << std::endl;
		for (int i = 0; i < argc; i++) {
			std::cerr << "argv[" << i << "] = " << argv[i] << std::endl;
		}
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "------------------" << std::endl;

		exit (EXIT_FAILURE);
	}

	while ((optchar = getopt(argc, argv, "n:d:p:t:l:")) != -1)
	{
		switch(optchar)
		{
		case 'n':
			n = atoi(optarg);
			break;
		case 'd':
			d = atoi(optarg);
			break;
		case 'p':
			pipelineLength = atoi(optarg);
			break;
		case 't':
			type = optarg;
			break;
		case 'l':
			library = optarg;
			break;
		default:
			std::cerr << "----- main() -----" << std::endl;
			std::cerr << "The passed argument is not valid." << std::endl;
			std::cerr << "The following arguments can and have to be passed:" << std::endl;
			std::cerr << "-n: Matrix size            (int)" << std::endl;
			std::cerr << "-d: Bandwidth              (int)" << std::endl;
			std::cerr << "-p: Pipeline length        (int)" << std::endl;
			std::cerr << "-t: Matrix type            (string)" << std::endl;
			std::cerr << "-l: Linear algebra library (string)" << std::endl;
			std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
			std::cerr << "------------------" << std::endl;

			exit (EXIT_FAILURE);
		}
	}

	if (d < 2)
	{
		std::cerr << "----- main() -----" << std::endl;
		std::cerr << "The matrix must have more sub-diagonals than a tri-diagonal matrix (d > 1). " << std::endl;
		std::cerr << "Instead, d = " << d << " has been passed." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "------------------" << std::endl;

		exit (EXIT_FAILURE);
	}

	if (d > n - 1)
	{
		std::cerr << "----- main() -----" << std::endl;
		std::cerr << "The matrix must be large enough to have d = " << d << " sub-diagonals (d < n). " << std::endl;
		std::cerr << "Instead, n = " << n << " and d = " << d << " have been passed." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "------------------" << std::endl;

		exit (EXIT_FAILURE);
	}

	if (pipelineLength < 1)
	{
		std::cerr << "----- main() -----" << std::endl;
		std::cerr << "At least one block update has to be performed in parallel (p > 0). " << std::endl;
		std::cerr << "Instead, p = " << pipelineLength << " has been passed." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "------------------" << std::endl;

		exit (EXIT_FAILURE);
	}

	/*
	 *
	 */
#ifdef OPENCL
	OpenClEnvironment* openClEnvironment = new OpenClEnvironment(CL_DEVICE_TYPE_GPU);
#endif

	/*
	 *
	 */
	Matrix<TYPE>* matrix = NULL;
#ifdef CUDA
	matrix = new Matrix<TYPE>(n, d);
#endif
#ifdef OPENCL
	matrix = new Matrix<TYPE>(n, d, openClEnvironment);
#endif
	if (strcmp(type, "i") == 0)
	{
		matrix->makeI();
	} else if (strcmp(type, "random") == 0) {
		matrix->makeRandom();
	} else if (strcmp(type, "toeplitz") == 0) {
		matrix->makeToeplitz();
	} else {
		std::cerr << "----- main() -----" << std::endl;
		std::cerr << "The following values are allowed for matrix type argument -t:" << std::endl;
		std::cerr << "- i" << std::endl;
		std::cerr << "- random" << std::endl;
		std::cerr << "- toeplitz" << std::endl;
		std::cerr << "Instead, \"" << type << "\" has been passed." << std::endl;
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "------------------" << std::endl;

		delete matrix;
#ifdef OPENCL
		delete openClEnvironment;
#endif

		exit (EXIT_FAILURE);
	}

	/*
	 *
	 */
	Solver<TYPE>* solver = NULL;
#ifdef CUDA
	if (strcmp(library, "cuBLAS") == 0)
		solver = new SolverCuBLAS<TYPE>(pipelineLength);
	else
	if (strcmp(library, "MAGMA") == 0)
		solver = new SolverMAGMA<TYPE>(pipelineLength);
	else
#endif
#ifdef OPENCL
	if (strcmp(library, "clBLAS") == 0)
		solver = new SolverClBLAS<TYPE>(n, pipelineLength, openClEnvironment);
	else
#endif
	{
		std::cerr << "----- main() -----" << std::endl;
		std::cerr << "The following values are allowed for library argument -l:" << std::endl;
#ifdef CUDA
		std::cerr << "- cuBLAS" << std::endl;
		std::cerr << "- MAGMA" << std::endl;
#endif
#ifdef OPENCL
		std::cerr << "- clBLAS" << std::endl;
#endif
		std::cerr << "Instead, \"" << library << "\" has been passed." << std::endl;
#ifndef CUDA
		std::cerr << "The following additional values are allowed for library argument -l if compiled with -D CUDA:" << std::endl;
		std::cerr << "- cuBLAS" << std::endl;
		std::cerr << "- MAGMA" << std::endl;
#endif
#ifndef OPENCL
		std::cerr << "The following additional values are allowed for library argument -l if compiled with -D OPENCL:" << std::endl;
		std::cerr << "- clBLAS" << std::endl;
#endif
		std::cerr << "EXECUTION WILL BE TERMINATED IMMEDIATELY" << std::endl;
		std::cerr << "------------------" << std::endl;

		delete matrix;
#ifdef OPENCL
		delete openClEnvironment;
#endif

		exit (EXIT_FAILURE);
	}

	/*
	 *
	 */
#ifdef OPENCL
	Runner<TYPE>* runner = new Runner<TYPE>(matrix, solver, pipelineLength, openClEnvironment);
#else
	Runner<TYPE>* runner = new Runner<TYPE>(matrix, solver, pipelineLength);
#endif

	std::cout << "memory footprint matrix: " << (matrix->getSize() / (TYPE)(1<<20)) << "MBytes" << std::endl;
	std::cout << "memory footprint solver: " << (solver->getSize() / (TYPE)(1<<20)) << "MBytes" << std::endl;
	std::cout << "memory footprint runner: " << (runner->getSize() / (TYPE)(1<<20)) << "MBytes" << std::endl;
	std::cout << "total memory footprint:  " << ((matrix->getSize() + solver->getSize() + runner->getSize()) / (TYPE)(1<<20)) << "MBytes" << std::endl;

	gettimeofday(&start, NULL);
	runner->run();
	gettimeofday(&end, NULL);

	elapsed = (TYPE)(end.tv_sec - start.tv_sec) + (TYPE)(end.tv_usec - start.tv_usec) * (TYPE)0.000001;

	// matrix->toConsole();
	// matrix->toCSV();

	delete runner;
	delete solver;
	delete matrix;
#ifdef OPENCL
	delete openClEnvironment;
#endif

	std::cout << "Runtime:                 " << elapsed << "s" << std::endl;

	return 0;
}
