/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "computeDummy.cuh"

template<class T>
__global__ void computeDummyKernel(T* data, const int numOfIterations)
{
	int idx;
	T x;

	idx = threadIdx.y * blockDim.x + threadIdx.x;
	x = data[idx % blockDim.x];

	for (int i = 0; i < numOfIterations; i++)
		x += (T)-1 + (i & 1) * (T)2;

	if (threadIdx.y == 0)
		data[idx] = x;
}

template __global__ void computeDummyKernel<float>(float* data, const int n);
template __global__ void computeDummyKernel<double>(double* data, const int n);
