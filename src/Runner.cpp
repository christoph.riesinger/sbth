/*
 * Copyright 2016 Christoph Riesinger
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

#include "Runner.hpp"

#ifdef CUDA
#include <cuda_runtime.h>
#endif

#include "common.h"

template <class T>
#ifdef OPENCL
Runner<T>::Runner(Matrix<T>* matrix,
		Solver<T>* solver,
		int pipelineLength,
		OpenClEnvironment* openClEnvironment) :
		matrix(matrix),
		solver(solver),
		pipelineLength(pipelineLength),
		openClEnvironment(openClEnvironment)
#else
Runner<T>::Runner(Matrix<T>* matrix,
		Solver<T>* solver,
		int pipelineLength) :
		matrix(matrix),
		solver(solver),
		pipelineLength(pipelineLength)
#endif
{
	int b;
	T zero, half, one, minus;

	b = computeB(0);

#ifdef OPENCL
	cl_int error;

	vMain = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	vSub = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	xMain = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	xSub = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	h = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	zSub = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	wMain = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	wSub = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, this->matrix->getD() * b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	norm = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	dot = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, b * sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	ZERO = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	HALF = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	ONE = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
	MINUS = clCreateBuffer(*this->openClEnvironment->getContext(), CL_MEM_READ_WRITE, sizeof(T), NULL, &error);
	OPENCL_ERROR_CHECK(error)
#else
	CUDA_ERROR_CHECK(cudaMalloc(&vMain, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&vSub, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&xMain, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&xSub, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&h, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&zSub, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&wMain, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&wSub, b * matrix->getD() * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&norm, b * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&dot, b * sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&ZERO, sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&HALF, sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&ONE, sizeof(T)))
	CUDA_ERROR_CHECK(cudaMalloc(&MINUS, sizeof(T)))
#endif

	zero = 0;
	half = -0.5;
	one = 1;
	minus = -1;

	solver->setVector(1, &zero, 1, ZERO, 0, 1, 0);
	solver->setVector(1, &half, 1, HALF, 0, 1, 0);
	solver->setVector(1, &one, 1, ONE, 0, 1, 0);
	solver->setVector(1, &minus, 1, MINUS, 0, 1, 0);
}

template <class T>
Runner<T>::~Runner()
{
#ifdef OPENCL
	OPENCL_ERROR_CHECK(clReleaseMemObject(MINUS))
	OPENCL_ERROR_CHECK(clReleaseMemObject(ONE))
	OPENCL_ERROR_CHECK(clReleaseMemObject(HALF))
	OPENCL_ERROR_CHECK(clReleaseMemObject(ZERO))
	OPENCL_ERROR_CHECK(clReleaseMemObject(dot))
	OPENCL_ERROR_CHECK(clReleaseMemObject(norm))
	OPENCL_ERROR_CHECK(clReleaseMemObject(wSub))
	OPENCL_ERROR_CHECK(clReleaseMemObject(wMain))
	OPENCL_ERROR_CHECK(clReleaseMemObject(zSub))
	OPENCL_ERROR_CHECK(clReleaseMemObject(h))
	OPENCL_ERROR_CHECK(clReleaseMemObject(xSub))
	OPENCL_ERROR_CHECK(clReleaseMemObject(xMain))
	OPENCL_ERROR_CHECK(clReleaseMemObject(vSub))
	OPENCL_ERROR_CHECK(clReleaseMemObject(vMain))
#else
	CUDA_ERROR_CHECK(cudaFree(MINUS))
	CUDA_ERROR_CHECK(cudaFree(ONE))
	CUDA_ERROR_CHECK(cudaFree(HALF))
	CUDA_ERROR_CHECK(cudaFree(ZERO))
	CUDA_ERROR_CHECK(cudaFree(dot))
	CUDA_ERROR_CHECK(cudaFree(norm))
	CUDA_ERROR_CHECK(cudaFree(wSub))
	CUDA_ERROR_CHECK(cudaFree(wMain))
	CUDA_ERROR_CHECK(cudaFree(zSub))
	CUDA_ERROR_CHECK(cudaFree(h))
	CUDA_ERROR_CHECK(cudaFree(xSub))
	CUDA_ERROR_CHECK(cudaFree(xMain))
	CUDA_ERROR_CHECK(cudaFree(vSub))
	CUDA_ERROR_CHECK(cudaFree(vMain))
#endif
}

template <class T>
int Runner<T>::computeB(const int nu)
{
	int b;

	b = ((matrix->getN() - nu - 2) / matrix->getD()) + 1;

	return b;
}

template <class T>
int Runner<T>::computeR(const int nu, const int b)
{
	int r;

	r = matrix->getN() - nu - (b - 1) * matrix->getD() - 1;

	return r;
}

template <class T>
int Runner<T>::computeIdx(const int nu, const int beta)
{
	int idx;

	idx = nu + 1 + beta * matrix->getD();

	return idx;
}

template <class T>
void Runner<T>::processBlockPairs(const int step)
{
	int nu;
	int idx, b, m, streamID;

	// ==================== Start processing diagonal block ====================
	// x_m = A_beta,beta * v_m
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step; beta += 2)
	{
		idx = computeIdx(nu, beta);
		b = computeB(nu);
		m = (beta < b - 1) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->symv(m, ONE, matrix->getA(), matrix->getOffset(idx, idx), 2 * matrix->getD() - 1, vMain, beta * matrix->getD(), 1, ZERO, xMain, beta * matrix->getD(), 1, streamID);

		nu--;
	}

	// w_m = x_m - 0.5 * (v_m^T * x_m) * v_m
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 1) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->dot(m, vMain, beta * matrix->getD(), 1, xMain, beta * matrix->getD(), 1, dot, beta, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 1) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->scal(1, HALF, 0, dot, beta, 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 1) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->copy(m, xMain, beta * matrix->getD(), 1, wMain, beta * matrix->getD(), 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 1) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->axpy(m, dot, beta, vMain, beta * matrix->getD(), 1, wMain, beta * matrix->getD(), 1, streamID);

		nu--;
	}

	// A_beta,beta = A_beta,beta - v_m * w_m ^ T - w_m * v_m ^ T
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step; beta += 2)
	{
		idx = computeIdx(nu, beta);
		b = computeB(nu);
		m = (beta < b - 1) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->syr2(m, MINUS, vMain, beta * matrix->getD(), 1, wMain, beta * matrix->getD(), 1, matrix->getA(), matrix->getOffset(idx, idx), 2 * matrix->getD() - 1, streamID);

		nu--;
	}
	// ===================== End processing diagonal block =====================

	// =================== Start processing subdiagonal block ==================
	// x_s = A_beta+1,beta * v_m
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		idx = computeIdx(nu, beta);
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->gemv(m, matrix->getD(), ONE, matrix->getA(), matrix->getOffset(idx + matrix->getD(), idx), 2 * matrix->getD() - 1, vMain, beta * matrix->getD(), 1, ZERO, xSub, beta * matrix->getD(), 1, NORMAL, streamID);

		nu--;
	}

	// h = A_beta+1,beta(0:m-1,0) - v_0 * x_s
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		idx = computeIdx(nu, beta);
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->copy(m, matrix->getA(), matrix->getOffset(idx + matrix->getD(), idx), 1, h, beta * matrix->getD(), 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->copy(1, vMain, beta * matrix->getD(), 1, norm, beta, 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->scal(1, MINUS, 0, norm, beta, 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->axpy(m, norm, beta, xSub, beta * matrix->getD(), 1, h, beta * matrix->getD(), 1, streamID);

		nu--;
	}

	/*
	 * Determine an Householder vector v_s which transforms h to
	 * (*, 0, ..., 0)
	 */
	// || h ||
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->nrm2(m, h, beta * matrix->getD(), 1, norm, beta, streamID);

		nu--;
	}
	// 1 / (h(0) + sgn(h(0)) * || h ||)
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->determineHouseholderScal(h, beta * matrix->getD(), norm, beta, h, beta * matrix->getD(), norm, beta, streamID);

		nu--;
	}
	// First element v_s(0) of Householder vector is always 1
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->copy(1, ONE, 0, 1, vSub, beta * matrix->getD(), 1, streamID);

		nu--;
	}
	// Copy remaining elements v_s(1:m) of Householder vector from h(1:m)
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->copy(m - 1, h, beta * matrix->getD() + 1, 1, vSub, beta * matrix->getD() + 1, 1, streamID);

		nu--;
	}
	// Scale elements v_s(1:m) of Householder vector
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->scal(m - 1, norm, beta, vSub, beta * matrix->getD() + 1, 1, streamID);

		nu--;
	}
	// Scale v_s to length sqrt(2)
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->nrm2(m, vSub, beta * matrix->getD(), 1, norm, beta, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->determineUnitScal(norm, beta, norm, beta, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->scal(m, norm, beta, vSub, beta * matrix->getD(), 1, streamID);

		nu--;
	}

	// z_s^T = v_s^T * A_beta+1,beta
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		idx = computeIdx(nu, beta);
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->gemv(m, matrix->getD(), ONE, matrix->getA(), matrix->getOffset(idx + matrix->getD(), idx), 2 * matrix->getD() - 1, vSub, beta * matrix->getD(), 1, ZERO, zSub, beta * matrix->getD(), 1, TRANSPOSE, streamID);

		nu--;
	}

	// w_s = z_s - (v_s^T * x_s) * v
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->dot(m, vSub, beta * matrix->getD(), 1, xSub, beta * matrix->getD(), 1, dot, beta, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->scal(1, MINUS, 0, dot, beta, 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->copy(matrix->getD(), zSub, beta * matrix->getD(), 1, wSub, beta * matrix->getD(), 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->axpy(matrix->getD(), dot, beta, vMain, beta * matrix->getD(), 1, wSub, beta * matrix->getD(), 1, streamID);

		nu--;
	}

	// A_beta+1,beta = A_beta+1,beta - x_s * v^T - v_s * w_s^T (two general rank-1 updates)
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		idx = computeIdx(nu, beta);
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->ger(m, matrix->getD(), MINUS, xSub, beta * matrix->getD(), 1, vMain, beta * matrix->getD(), 1, matrix->getA(), matrix->getOffset(idx + matrix->getD(), idx), 2 * matrix->getD() - 1, streamID);

		nu--;
	}
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		idx = computeIdx(nu, beta);
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->ger(m, matrix->getD(), MINUS, vSub, beta * matrix->getD(), 1, wSub, beta * matrix->getD(), 1, matrix->getA(), matrix->getOffset(idx + matrix->getD(), idx), 2 * matrix->getD() - 1, streamID);

		nu--;
	}

	// v = v_s
	nu = (step - 1)>>1;
	for (int beta = (step & 1) ? 0 : 1; computeIdx(nu, beta) < matrix->getN() && beta < step && beta < computeB(nu) - 1; beta += 2)
	{
		b = computeB(nu);
		m = (beta < b - 2) ? matrix->getD() : computeR(nu, b);
		streamID = (beta>>1) % pipelineLength;

		solver->copy(m, vSub, beta * matrix->getD(), 1, vMain, (beta + 1) * matrix->getD(), 1, streamID);

		nu--;
	}
	// ==================== End processing subdiagonal block ===================
}

template <class T>
T Runner<T>::getSize()
{
	T size;

	size = ((8 * matrix->getD() + 2) * computeB(0) + 4) * sizeof(T);

	return size;
}

template <class T>
void Runner<T>::run()
{
	int nu;
	int b, m;

	for (int step = 0; step < 2 * (matrix->getN() - 2); step++)
	{
		nu = step>>1;

		if (!(step & 1))
		{
			b = computeB(nu);
			m = (nu < matrix->getN() - matrix->getD()) ? matrix->getD() : computeR(nu, b);

			/*
			 * Determine an Householder vector v which transforms
			 * A(nu+1:nu+m,nu) to (*, 0, ..., 0)
			 */
			// || A(nu+1:nu+m,nu) ||
			solver->nrm2(m, matrix->getA(), matrix->getOffset(nu + 1, nu), 1, norm, 0, 0);
			// 1 / (A(nu+1,nu) + sgn(A(nu+1,nu)) * || A(nu+1:nu+m,nu) ||)
			solver->determineHouseholderScal(matrix->getA(), matrix->getOffset(nu + 1, nu), norm, 0, matrix->getA(), matrix->getOffset(nu + 1, nu), norm, 0, 0);
			// First element v(0) of Householder vector is always 1
			solver->copy(1, ONE, 0, 1, vMain, 0, 1, 0);
			// Copy remaining elements v(1:m) of Householder vector from A(nu+2:nu+m,nu)
			solver->copy(m - 1, matrix->getA(), matrix->getOffset(nu + 2, nu), 1, vMain, 1, 1, 0);
			// Scale elements v(1:m) of Householder vector
			solver->scal(m - 1, norm, 0, vMain, 1, 1, 0);
			// Scale v to length sqrt(2)
			solver->nrm2(m, vMain, 0, 1, norm, 0, 0);
			solver->determineUnitScal(norm, 0, norm, 0, 0);
			solver->scal(m, norm, 0, vMain, 0, 1, 0);
			// Eliminate nu-th column by setting it to (*, 0, ..., 0)
			solver->copy(m - 1, ZERO, 0, 0, matrix->getA(), matrix->getOffset(nu + 2, nu), 1, 0);
		}

		processBlockPairs(nu);

		solver->synchronizeDevice();
	}
}

template class Runner<float>;
template class Runner<double>;
