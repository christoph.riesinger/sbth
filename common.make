################################################################################
# paths, directories and folders
################################################################################

BINDIR			:=	bin
OBJDIR			:=	obj

ifeq ($(API), CUDA)
GPUBINDIR		:=	$(GPUINSTALLPATH)/bin
endif

CXXINCLUDES		+=	-I$(GPUINSTALLPATH)/include
ifeq ($(API), CUDA)
CXXINCLUDES		+=	-I$(MAGMAINSTALLPATH)/include
NVCCINCLUDES	+=	-I$(GPUINSTALLPATH)/include
endif
ifeq ($(API), OPENCL)
CXXINCLUDES		+=	-I$(CLBLASINSTALLPATH)/include
endif

CXXLIBDIR		+=	-L$(GPUINSTALLPATH)/lib64 \
					-Wl,-rpath=$(GPUINSTALLPATH)/lib64
ifeq ($(API), CUDA)
CXXLIBDIR		+=	-L$(MAGMAINSTALLPATH)/lib \
					-Wl,-rpath=$(MAGMAINSTALLPATH)/lib
NVCCLIBDIR		+=	-L$(GPUINSTALLPATH)/lib64
endif
ifeq ($(API), OPENCL)
CXXLIBDIR		+=	-L$(CLBLASINSTALLPATH)/lib64 \
					-Wl,-rpath=$(CLBLASINSTALLPATH)/lib64
endif

CXXLIB			+=	-lrt
ifeq ($(API), CUDA)
CXXLIB			+=	-lcublas \
					-lmagma
NVCCLIB			+=	-lcudart
endif
ifeq ($(API), OPENCL)
CXXLIB			+=	-lclBLAS \
					-lOpenCL
endif

ifeq ($(API), CUDA)
	EXECUTABLE	:=	sbth_cuda
endif
ifeq ($(API), OPENCL)
	EXECUTABLE	:=	sbth_opencl
endif

################################################################################
# source files
################################################################################

# c/c++ source files (compiled with $(CXX))
CXXFILES		:=	src/Matrix.cpp \
					src/Runner.cpp
ifeq ($(API), CUDA)
CXXFILES		+=	src/SolverCuBLAS.cpp \
					src/SolverMAGMA.cpp
endif
ifeq ($(API), OPENCL)
CXXFILES		+=	src/OpenClEnvironment.cpp \
					src/OpenClInterface.cpp \
					src/SolverClBLAS.cpp
endif
CXXFILES		+=	src/main.cpp

# cuda source files (compiled with $(NVCC))
ifeq ($(API), CUDA)
NVCCFILES		:=	src/computeDummy.cu \
					src/determineHouseholderScal.cu \
					src/determineUnitScal.cu \
					src/CudaInterface.cu
endif

################################################################################
# compiler arguments and flags
################################################################################

CXXFLAGS		+=	-O3 \
					-Wall \
					-D $(API)

# arch: specifies the compatibility from source code to PTX stage. Can be a
#       virtual (compute_*) or real (sm_*) compatibility.
# code: specifies the compatibility from PTX stage to binary code. Can only be
#       real (sm_*). Code has to be >= arch.
# -rdc: -rdc is short for --relocatable-device-code which generates relocatable
#       device code. This is necessary to generate multiple CUDA object files
#       which can then be linked together.
ifeq ($(API), CUDA)
NVCCFLAGS		+=	-lineinfo \
					-rdc=true \
					-use_fast_math \
					--compiler-options -Wall \
					-D $(API)
endif

################################################################################
# linker arguments and flags
################################################################################

LINKERFLAGS		+=	

# -dlink: Necessary linker option to link multiple CUDA object files together.
ifeq ($(API), CUDA)
NVCCLINKERFLAGS	+=	-dlink
endif

################################################################################
# set up virtual path to enable subfolders for source files
################################################################################

VPATH 			:=	src/

################################################################################
# set up object files
#
# semantics patsubst(a, b, c): replace b by a in c.
################################################################################

CXXOBJS			:=	$(patsubst %.cpp, $(OBJDIR)/%.cpp.o, $(notdir $(CXXFILES)))
ifeq ($(API), CUDA)
NVCCOBJS		:=	$(patsubst %.cu,  $(OBJDIR)/%.cu.o,  $(notdir $(NVCCFILES)))
endif

OBJS			:=  $(CXXOBJS)
ifeq ($(API), CUDA)
OBJS			+=  $(NVCCOBJS)
endif

################################################################################
# set up link process
################################################################################

ifeq ($(API), CUDA)
LINKLINE		:=	$(LINKER) $(LINKERFLAGS) $(OBJS) $(OBJDIR)/cuda.cu.o -o $(BINDIR)/$(EXECUTABLE) $(CXXLIBDIR) $(NVCCLIBDIR) $(CXXLIB) $(NVCCLIB)
NVCCLINKLINE	:=	$(NVCCLINKER) $(NVCCLINKERFLAGS) $(NVCCOBJS) -o $(OBJDIR)/cuda.cu.o
endif
ifeq ($(API), OPENCL)
LINKLINE		:=	$(LINKER) $(LINKERFLAGS) $(OBJS) -o $(BINDIR)/$(EXECUTABLE) $(CXXLIBDIR) $(GPULIBDIR) $(CXXLIB) $(GPULIB)
endif

################################################################################
# targets
################################################################################

# target to compile c++ files
$(OBJDIR)/%.cpp.o: %.cpp
	$(CXX) $(CXXINCLUDES) $(CXXFLAGS) -c $< -o $@

# target to compile cuda files
ifeq ($(API), CUDA)
$(OBJDIR)/%.cu.o: %.cu
	$(NVCC) $(NVCCINCLUDES) $(NVCCFLAGS) -c $< -o $@
endif
	
# misc targets
makedirectories:
	mkdir -p $(OBJDIR)
	mkdir -p $(BINDIR)

clean:
	rm -f $(OBJDIR)/*
#	rm -f $(BINDIR)/*
	rmdir $(OBJDIR)
#	rmdir $(BINDIR)
	
ifneq ($(CORRECTAPI), 1)
guard:
	$(error API argument has to be either CUDA or OPENCL, according to utilized GPU)
else
guard:
	
endif
	
# link targets (results are executables)
ifeq ($(API), CUDA)
link: makedirectories $(CXXOBJS) nvccobject
	@echo '-- Invoking C++ linker: Link C++ objects, CUDA objects and single CUDA object --'
	$(LINKLINE)
	@echo '-- End invoking C++ linker --'
endif
ifeq ($(API), OPENCL)
link: makedirectories $(CXXOBJS)
	@echo '-- Invoking C++ linker: Link C++ objects --'
	$(LINKLINE)
	@echo '-- End invoking C++ linker --'
endif
	
# compile targets (results are object files)
ifeq ($(API), CUDA)
nvccobject: $(NVCCOBJS)
	@echo '-- Invoking CUDA linker: Linking all CUDA objects to one single object --'
	$(NVCCLINKLINE)
	@echo '-- End invoking CUDA linker --'
endif

# frontend targets (sould be called as make option)
all: guard link
	@echo '-- Everything went fine --'

