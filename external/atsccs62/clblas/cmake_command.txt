cmake -DBUILD_KTEST=OFF -DBUILD_TEST=OFF -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc -DCMAKE_Fortran_COMPILER=gfortran -DCMAKE_INSTALL_PREFIX=~/dissertation/sbth/external/atsccs62/clblas/ -DOPENCL_VERSION=1.2 src/
CC=gcc CXX=g++ make -j$OMP_NUM_THREADS install
