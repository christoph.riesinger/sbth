################################################################################
# guard to make sure that either CUDA or OpenCL is selected as API
################################################################################

CORRECTAPI	:= 0
ifeq ($(API), CUDA)
CORRECTAPI	:= 1
endif

################################################################################
# paths, directories and folders
################################################################################

GPUINSTALLPATH		:=	$(CUDATOOLKIT_HOME)
MAGMAINSTALLPATH	:=	external/daint/magma

CXXINCLUDES			:=	
NVCCINCLUDES		:=	
CXXLIBDIR			:=	
NVCCLIBDIR			:=	
CXXLIB				:=	
NVCCLIB				:=	
COMPUTE_CAPABILITY	:=	60

################################################################################
# compilers and linkers
################################################################################

CXX					:=	icpc
LINKER				:=	icpc
NVCC				:=	nvcc
NVCCLINKER			:=	nvcc

################################################################################
# compiler arguments and flags
################################################################################

CXXFLAGS			:=	

# arch: specifies the compatibility from source code to PTX stage. Can be a
#       virtual (compute_*) or real (sm_*) compatibility.
# code: specifies the compatibility from PTX stage to binary code. Can only be
#       real (sm_*). Code has to be >= arch.
# -rdc: -rdc is short for --relocatable-device-code which generates relocatable
#       device code. This is necessary to generate multiple CUDA object files
#       which can then be linked together.
NVCCFLAGS			:=	-ccbin=$(CXX) \
						-O3 \
						-gencode arch=compute_$(COMPUTE_CAPABILITY),code=sm_$(COMPUTE_CAPABILITY)
#						--ptxas-options -v

################################################################################
# linker arguments and flags
################################################################################

LINKERFLAGS			:=	

# -dlink: Necessary linker option to link multiple CUDA object files together.
NVCCLINKERFLAGS		:=	-arch=sm_$(COMPUTE_CAPABILITY)

include common.make

