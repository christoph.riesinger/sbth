################################################################################
# guard to make sure that either CUDA or OpenCL is selected as API
################################################################################

CORRECTAPI	:= 0
ifeq ($(API), CUDA)
CORRECTAPI	:= 1
endif
ifeq ($(API), OPENCL)
CORRECTAPI	:= 1
endif

################################################################################
# paths, directories and folders
################################################################################

GPUINSTALLPATH		:=	/usr
ifeq ($(API), CUDA)
MAGMAINSTALLPATH	:=	external/atsccs62/magma
endif
ifeq ($(API), OPENCL)
CLBLASINSTALLPATH	:=	external/atsccs62/clblas
endif

ifeq ($(API), CUDA)
CXXINCLUDES			:=	
NVCCINCLUDES		:=	
endif
ifeq ($(API), OPENCL)
CXXINCLUDES			:=	
endif

ifeq ($(API), CUDA)
CXXLIBDIR			:=	-L$(GPUINSTALLPATH)/lib/x86_64-linux-gnu \
						-Wl,-rpath=$(GPUINSTALLPATH)/lib/x86_64-linux-gnu
NVCCLIBDIR			:=	
endif
ifeq ($(API), OPENCL)
CXXLIBDIR			:=	
endif

ifeq ($(API), CUDA)
CXXLIB				:=	
NVCCLIB				:=	
endif
ifeq ($(API), OPENCL)
CXXLIB				:=	
endif

ifeq ($(API), CUDA)
COMPUTE_CAPABILITY	:=	50
endif

################################################################################
# compilers and linkers
################################################################################

ifeq ($(API), CUDA)
CXX					:=	g++-4.9
LINKER				:=	g++-4.9
endif
ifeq ($(API), OPENCL)
CXX					:=	g++
LINKER				:=	g++
endif

ifeq ($(API), CUDA)
NVCC				:=	nvcc
NVCCLINKER			:=	nvcc
endif

################################################################################
# compiler arguments and flags
################################################################################

CXXFLAGS			:=	

# arch: specifies the compatibility from source code to PTX stage. Can be a
#       virtual (compute_*) or real (sm_*) compatibility.
# code: specifies the compatibility from PTX stage to binary code. Can only be
#       real (sm_*). Code has to be >= arch.
# -rdc: -rdc is short for --relocatable-device-code which generates relocatable
#       device code. This is necessary to generate multiple CUDA object files
#       which can then be linked together.
ifeq ($(API), CUDA)
NVCCFLAGS			:=	-ccbin=$(CXX) \
						-O3 \
						-gencode arch=compute_$(COMPUTE_CAPABILITY),code=sm_$(COMPUTE_CAPABILITY)
#						--ptxas-options -v
endif

################################################################################
# linker arguments and flags
################################################################################

LINKERFLAGS			:=	

# -dlink: Necessary linker option to link multiple CUDA object files together.
ifeq ($(API), CUDA)
NVCCLINKERFLAGS		:=	-arch=sm_$(COMPUTE_CAPABILITY)
endif

include common.make

