#!/usr/bin/env python
import csv
import os
import os.path
import subprocess
import sys

matrixType = 'toeplitz'
ns = [5000]
ds = [16, 32, 64, 128, 256]
pipelineLengths = range(1, 13)
libraries = [('cuBLAS', 'cuda')]

# Run benchmarks
for n in ns:
	for d in ds:
		for pipelineLength in pipelineLengths:
			for library in libraries:
				sys.stdout.write('n = ' + str(n) + ', d = ' + str(d) + ', p = ' + str(pipelineLength) + ', l = ' + library[0] + ' ...')
				sys.stdout.flush()
				
				outputFilePath = os.environ['HOME'] + '/workspace/sbth/benchmark/benchmark_' + str(n) + '_' + str(d) + '_' + str(pipelineLength) + '_' + library[0] + '.txt'
				outputFile = open(outputFilePath, 'w')
				subprocess.call([os.environ['HOME'] + '/workspace/sbth/bin/sbth_' + library[1], '-n', str(n), '-d', str(d), '-p', str(pipelineLength), '-t', matrixType, '-l',  library[0]], stdout=outputFile)
				outputFile.close()
				
				sys.stdout.write(' finished\n')
				sys.stdout.flush()

# Evaluate benchmarks
for n in ns:
	for library in libraries:
		data = [[''] + ds]
		for pipelineLength in pipelineLengths:
			localData = [0] * len(data[0])
			localData[0] = pipelineLength
			i = 1;
			for d in ds:
				inputFilePath = os.environ['HOME'] + '/workspace/sbth/benchmark/benchmark_' + str(n) + '_' + str(d) + '_' + str(pipelineLength) + '_' + library[0] + '.txt'
				if (os.path.isfile(inputFilePath)):
					inputFile = open(inputFilePath, 'r')
					for lineNumber, line in enumerate(inputFile):
						if (lineNumber == 4):
							localData[i] = 0.1 * float(line.split()[1][:-1])
					inputFile.close()
				i += 1
			data.append(localData)
		outputFile = open(os.environ['HOME'] + '/workspace/sbth/benchmark/' + str(n) + '_' + library[0] + '_kepler.csv', 'w')
		outputFileWriter = csv.writer(outputFile, delimiter=';')
		outputFileWriter.writerows(data)
		outputFile.close()

