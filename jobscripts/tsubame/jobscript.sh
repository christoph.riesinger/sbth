#!/bin/sh
#
#PBS -o /home/usr7/16ITA238/workspace/sbth/results/sbth_cuda.o.txt
#PBS -e /home/usr7/16ITA238/workspace/sbth/results/sbth_cuda.e.txt
#
#PBS -m ae
#PBS -M riesinge@in.tum.de

source set_intel-2015.2.164.sh
source set_cuda-7.5.sh

cd ~/workspace/sbth
python jobscripts/tsubame/batch.py

