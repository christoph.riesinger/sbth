#!/bin/bash

#SBATCH -D /home/hpc/pr63so/lu32dec/workspace/sbth/
#SBATCH -o /home/hpc/pr63so/lu32dec/workspace/sbth/results/sbth_cuda.o.txt
#SBATCH -e /home/hpc/pr63so/lu32dec/workspace/sbth/results/sbth_cuda.e.txt
#SBATCH -J sbth_cuda
#SBATCH --get-user-env
#
#SBATCH --partition=nvd
#Total number of tasks/ranks/processes:
#SBATCH --ntasks=1
#Number of tasks/ranks/processes per node:
#SBATCH --ntasks-per-node=1
#Number of threads per task/rank/process:
#SBATCH --cpus-per-task=16
#SBATCH --time=24:00:00
#
#SBATCH --constraint=turbo_off
#
#SBATCH --mail-type=end
#SBATCH --mail-user=riesinge@in.tum.de

python jobscripts/mac-cluster-nvd/batch.py

