#!/bin/bash -l

#SBATCH -D /users/riesinge/workspace/sbth/
#SBATCH -o /users/riesinge/workspace/sbth/results/sbth_cuda.o.txt
#SBATCH -e /users/riesinge/workspace/sbth/results/sbth_cuda.e.txt
#SBATCH -J sbth_cuda
#
#SBATCH --constraint=gpu
#SBATCH --partition=normal
#Total number of tasks/ranks/processes:
#SBATCH --ntasks=1
#Number of tasks/ranks/processes per node:
#SBATCH --ntasks-per-node=1
#Number of threads per task/rank/process:
#SBATCH --cpus-per-task=12
#SBATCH --time=24:00:00
#
#SBATCH --mail-type=end
#SBATCH --mail-user=riesinge@in.tum.de

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

python jobscripts/daint/batch.py

